/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.login;

import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author INTERNET
 */
// @Named(value = "usuario") este no funciona porque no se usa CFI
@ManagedBean(name="usuario")
@Dependent
public class Usuario {

    private String usuario;

    private String contrasena;

    private String mensaje_respuesta;

    /**
     * Creates a new instance of Usuario
     */
    public Usuario() {
    }

    public String loginInfo() {
        if (this.getUsuario().equals("admin") && this.getContrasena().equals("admin")) {
            this.setMensaje_respuesta("Usuario es correcto");
        } else if (this.getUsuario().equals("") && this.getContrasena().equals("")) {
            this.setMensaje_respuesta("Por favor ingrese nombre y contraseña");
        } else {
            this.setMensaje_respuesta("Usuario incorrecto");
        }
        
        return getMensaje_respuesta();
    }

    /**
     * Get the value of mensaje_respuesta
     *
     * @return the value of mensaje_respuesta
     */
    public String getMensaje_respuesta() {
        return mensaje_respuesta;
    }

    /**
     * Set the value of mensaje_respuesta
     *
     * @param mensaje_respuesta new value of mensaje_respuesta
     */
    private void setMensaje_respuesta(String mensaje_respuesta) {
        this.mensaje_respuesta = mensaje_respuesta;
    }

    /**
     * Get the value of contrasena
     *
     * @return the value of contrasena
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * Set the value of contrasena
     *
     * @param contrasena new value of contrasena
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * Get the value of usuario
     *
     * @return the value of usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Set the value of usuario
     *
     * @param usuario new value of usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

}
