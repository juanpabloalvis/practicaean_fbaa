/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.login;

import com.fbaa.formularios.NavegacionBean;
import com.fbaa.jpa.DAO.GestionarUsuarioAutenticadoDAO;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author INTERNET
 */
// @Named(value = "usuario") este no funciona porque no se usa CFI
@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private static final long serialVersionUID = 2L;

    private String usuario;
    private String contrasena;
    private String mensaje_respuesta;
    private boolean loggedIn;
    private final GestionarUsuarioAutenticadoDAO usuariosAutenticados = new GestionarUsuarioAutenticadoDAO();

    // TODO revisar si se puede utilizar el operador New()
    @ManagedProperty(value = "#{navegacionBean}")
    private final NavegacionBean navegacionBean = new NavegacionBean();

    /**
     * Creates a new instance of Usuario
     */
    public LoginBean() {
    }

    public String doLogin() throws NoSuchAlgorithmException {
        InputStream in = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            in = IOUtils.toInputStream(contrasena, "UTF-8");

            byte byteData[] = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            // Get every user from our sample database :)
            if (usuariosAutenticados.usuarioAutenticado(usuario, sb.toString()) != null) {
                loggedIn = true;
                return navegacionBean.redirectToIndex();
            }   // Set login ERROR

        } catch (IOException ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        FacesMessage msg = new FacesMessage("Error de autenticación!", "ERROR MSG");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        // To to login page
        return navegacionBean.toLogin();
    }

    /**
     * Logout operation.
     *
     * @return
     */
    public String doLogout() {
        // Set the paremeter indicating that user is logged in to false
        loggedIn = false;
        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        this.setMensaje_respuesta("Usuario es correcto");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return navegacionBean.toIndex();
    }

    public void setNavegacionBean(NavegacionBean navegacionBean) {
        navegacionBean = navegacionBean;
    }

    public String loginInfo() {
        if (this.getUsuario().equals("admin") && this.getContrasena().equals("admin")) {
            this.setMensaje_respuesta("Usuario es correcto");
        } else if (this.getUsuario().equals("") && this.getContrasena().equals("")) {
            this.setMensaje_respuesta("Por favor ingrese nombre y contraseña");
        } else {
            this.setMensaje_respuesta("Usuario incorrecto");
        }

        return getMensaje_respuesta();
    }

    /**
     * Get the value of mensaje_respuesta
     *
     * @return the value of mensaje_respuesta
     */
    public String getMensaje_respuesta() {
        return mensaje_respuesta;
    }

    /**
     * Set the value of mensaje_respuesta
     *
     * @param mensaje_respuesta new value of mensaje_respuesta
     */
    private void setMensaje_respuesta(String mensaje_respuesta) {
        this.mensaje_respuesta = mensaje_respuesta;
    }

    /**
     * Get the value of contrasena
     *
     * @return the value of contrasena
     */
    public String getContrasena() {
        return contrasena;
    }

    /**
     * Set the value of contrasena
     *
     * @param contrasena new value of contrasena
     */
    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    /**
     * Get the value of usuario
     *
     * @return the value of usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Set the value of usuario
     *
     * @param usuario new value of usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
}
