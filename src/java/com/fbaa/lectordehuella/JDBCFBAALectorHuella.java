/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.lectordehuella;

import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.fbaa.servlets.IdAndHuella;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author INTERNET
 */
public class JDBCFBAALectorHuella {

    private static String db_URL;
    private static String user_Mysql;
    private static String pw_Mysql;

    static {
        Properties prop = new Properties();

        Path currentRelativePath = Paths.get("");

        try {
            prop.load(new FileInputStream(currentRelativePath.toAbsolutePath().toString() + File.separator + "confMysql.properties"));
            db_URL = prop.getProperty("db_URL");
            user_Mysql = prop.getProperty("ipMysql");
            pw_Mysql = prop.getProperty("puertoMysql");
        } catch (IOException e) {
            System.err.println("--" + e);
        }
    }

    // JDBC driver name and database URL
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String SQL_TO_WRITE_OBJECT = "INSERT INTO huella_info (ID, CampoHuella) VALUES(?, ?) ON DUPLICATE KEY UPDATE CampoHuella =values(CampoHuella)";
    private static final String SQL_TO_WRITE_REGISTER = "INSERT INTO huella_registros (id_huella, HoraRegistro) VALUES(?, ?);";
    private static final String SQL_TO_GET_ALL_FINGERPRINTS = "SELECT id, CampoHuella FROM huella_info";

    //  Database credentials
    private static String DB_URL = "";
    private static String USER = "";
    private static String PASS = "";
    public static final int MYSQL_DUPLICATE_PK = 1062;
    public static final int MYSQL_CONSTRAIN_VIOLATION = 1452;

    static {
        Properties prop = new Properties();

        try {
            prop.load(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("confMysql.properties"));
            DB_URL = prop.getProperty("db_URL");
            USER = prop.getProperty("user_Mysql");
            PASS = prop.getProperty("pw_Mysql");

        } catch (IOException ex) {
            Logger.getLogger(JDBCFBAALectorHuella.class.getName()).log(Level.SEVERE, null, ex);
        }

//        String initParameter = prop.getProperty("db_URL");
//        System.out.println("---> FUNCIONA o NO" + initParameter);
    }

    public static String ingresarPlantilla(String cedula, DPFPTemplate plantilla) {
        String respuesta = "Se ha ingresado/actualizado exitosamente el usuario con cédula: " + cedula;
        Connection conn = null;
        Statement stmt = null;

        try {
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            PreparedStatement pstmt = conn.prepareStatement(SQL_TO_WRITE_OBJECT, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, cedula);
            ByteArrayInputStream fingerprintData = new ByteArrayInputStream(plantilla.serialize());
            int fingerprintDataLength;
            fingerprintDataLength = plantilla.serialize().length;
            pstmt.setBinaryStream(2, fingerprintData, fingerprintDataLength);

            pstmt.execute();

        } catch (ClassNotFoundException se) {
            respuesta = "Ocurrió un error guardando la huella: " + se;
        } catch (SQLException sqlEx) {
            //System.err.println("==="+se);
            if (sqlEx.getErrorCode() == MYSQL_DUPLICATE_PK) {
                respuesta = "Esta huella ya se encuentra registrada: ";
            } else if (sqlEx.getErrorCode() == MYSQL_CONSTRAIN_VIOLATION) {
                respuesta = "La cédula " + cedula + " no se encuentra registrada como voluntario";

            } else {
                respuesta = "Ocurrió un error guardando la huella: ";
            }
            System.out.println("" + sqlEx);
            System.out.println("" + sqlEx.getErrorCode());
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException se) {
            }// do nothing
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                respuesta = "Ocurrió un error guardando la huella";
            }//end finally try

        }//end try
        return respuesta;
    }//end main

    public static String registrarHora(String cedula) {
        String respuesta = "Se ha registrado el tiempo para el usuario con cédula: " + cedula;
        Connection conn = null;
        Statement stmt = null;
        try {
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            PreparedStatement pstmt = conn.prepareStatement(SQL_TO_WRITE_REGISTER, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, cedula);
//            GregorianCalendar calendar = new GregorianCalendar();
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String string = format.format(calendar.getTime());

            java.util.Date today = new java.util.Date();
            java.sql.Timestamp timestamp = new java.sql.Timestamp(today.getTime());
            pstmt.setTimestamp(2, timestamp);
            pstmt.execute();
        } catch (SQLException | ClassNotFoundException se) {
            respuesta = "Ocurrió un error registrando la huella";
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                respuesta = "Ocurrió un error registrando la huella";
            }// do nothing
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {

                respuesta = "Ocurrió un error registrando la huella";
            }//end finally try
        }//end try
        return respuesta;
    }//end main

    public static ArrayList<IdAndHuella> getListaHuellas() {
        ArrayList<IdAndHuella> arrayList = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        IdAndHuella idAndHuella = new IdAndHuella();
        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            //STEP 4: Execute a query
            stmt = conn.createStatement();

            ResultSet rs = stmt.executeQuery(SQL_TO_GET_ALL_FINGERPRINTS);
            DPFPTemplate referenceTemplate = null;
            //STEP 5: Extract data from result set
            while (rs.next()) {
                // TODO en caso de haber lentitud en la consulta, 
                // se debe validar que únicamente se traigan huellas de personas 
                // que estén activas para voluntariado.

                String cedula = rs.getString("id");
                referenceTemplate = null;
                byte templateBuffer[] = rs.getBytes("CampoHuella");
                //Crea una nueva plantilla a partir de la guardada en la base de datos
                referenceTemplate = DPFPGlobal.getTemplateFactory().createTemplate(templateBuffer);
                idAndHuella = new IdAndHuella(cedula, referenceTemplate);
                arrayList.add(idAndHuella);

            }
            rs.close();
        } catch (SQLException se) {
        } catch (ClassNotFoundException | IllegalArgumentException e) {
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) {
                    conn.close();
                }
            } catch (SQLException se) {
            }// do nothing
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
            }//end finally try
        }//end try
        return arrayList;
    }

}
