/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.lectordehuella;

import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.capture.DPFPCapture;
import com.digitalpersona.onetouch.capture.DPFPCapturePriority;
import com.digitalpersona.onetouch.capture.event.DPFPDataEvent;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusEvent;
import com.digitalpersona.onetouch.capture.event.DPFPReaderStatusListener;
import com.digitalpersona.onetouch.processing.DPFPEnrollment;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author INTERNET
 */
public class AplicacionDeEjemplo {

    public final int NUMERO_DE_VECES_NECESARIAS_PARA_REGISTRAR_HUELLA = 3;
    private final static String DEFAULT_UI_FACTORY = "com.digitalpersona.onetouch.sampleapp.ConsoleUserInterfaceFactory";
    private final static String DEFAULT_DB_FACTORY = "com.digitalpersona.onetouch.sampleapp.SessionUserDatabaseFactory";

    //Varible que permite iniciar el dispositivo de lector de huella conectado
    // con sus distintos metodos.
    private final DPFPCapture lector = DPFPGlobal.getCaptureFactory().createCapture();
    //Varible que permite establecer las capturas de la huellas, para determina sus caracteristicas
    // y poder estimar la creacion de un template de la huella para luego poder guardarla
    private final DPFPEnrollment registrador = DPFPGlobal.getEnrollmentFactory().createEnrollment();
    private final DPFPFeatureExtraction featureExtractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();

    //Esta variable tambien captura una huella del lector y crea sus caracteristcas para auntetificarla
    // o verificarla con alguna guarda en la BD
    private final DPFPVerification verificador = DPFPGlobal.getVerificationFactory().createVerification();

    // Objeto que gurada la muestra
    private DPFPSample muestraEjemplo = null;

    public void cargarCrearLectorHuella() {
        lector.addReaderStatusListener(new DPFPReaderStatusListener() {

            @Override
            public void readerConnected(DPFPReaderStatusEvent dpfprs) {
                System.out.println("Lector listo para guardar voluntarios");
            }

            @Override
            public void readerDisconnected(DPFPReaderStatusEvent dpfprs) {
                System.out.println("El lector se ha desconectado");
            }
        });

        // configurar prioridad de captura
        lector.setPriority(DPFPCapturePriority.CAPTURE_PRIORITY_LOW);

    }

    public void escucharParaRegistrarUsuario() {
        // Thread.sleep(4000);
        System.out.println("Escuchando..");
        System.out.println("...->" + registrador.getFeaturesNeeded());
        try {
            while (registrador.getFeaturesNeeded() > 0) {
                //System.out.println("Muestra " + (registrador.getFeaturesNeeded() + 1) + " de " + NUMERO_DE_VECES_NECESARIAS_PARA_REGISTRAR_HUELLA);

                DPFPSample sample = getHuellaDeMuestra();
                if (sample == null) {
                    continue;
                }
                DPFPFeatureSet featureSet = null;
                try {
                    featureSet = featureExtractor.createFeatureSet(sample, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);
                } catch (DPFPImageQualityException ex) {
                    System.out.printf("Mala calidad de imagen de la huella: \"%s\". Intente nuevamente. \n", ex.getCaptureFeedback().toString());
                }
                registrador.addFeatures(featureSet);

                //  registrador.addFeatures(featureSet);
            }//fin while

        } catch (DPFPImageQualityException ex) {
            System.out.printf("Falló registrar la huella.\n");
        }
        // TODO esta plantilla sería la que se debe guardar en la base de datos.
        DPFPTemplate template = registrador.getTemplate();
        JDBCFBAALectorHuella.ingresarPlantilla("4", template);
        //JDBCFBAALectorHuellaV2.ingresarPlantilla("4", template);
        System.out.println("Registro Existoso");
    }

    public void iniciarCaptura() {
        lector.startCapture();

    }

    public void detenerCaptura() {
        lector.stopCapture();
    }

    private DPFPSample getHuellaDeMuestra() {
        // final DPFPSample muestraEjemplo = null;
        lector.addDataListener((DPFPDataEvent e) -> {
            if (e != null && e.getSample() != null) {
                try {
                    // Obtiene una muestra de una huella
                    muestraEjemplo = e.getSample();
                    // Obtiene las caractereisticas de la huella
                    DPFPFeatureSet featureSet = featureExtractor.createFeatureSet(muestraEjemplo, DPFPDataPurpose.DATA_PURPOSE_ENROLLMENT);
                } catch (DPFPImageQualityException ex) {
                    System.out.println("Mala calidad de la huella");
                }

            }
        });
        return muestraEjemplo;
    }

    public int getValidarUsuario() {
        int cedula = 0;
        /* antes de prueba        try {

         DPFPSample sample = getHuellaDeMuestra();

         //DPFPFeatureExtraction featureExtractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
         DPFPFeatureSet featureSet = featureExtractor.createFeatureSet(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

         DPFPVerification matcher = DPFPGlobal.getVerificationFactory().createVerification();
         matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

         } catch (DPFPImageQualityException ex) {
         Logger.getLogger(AplicacionDeEjemplo.class.getName()).log(Level.SEVERE, null, ex);
         }
         despues de prueba       
         */
        return cedula;
    }
}
