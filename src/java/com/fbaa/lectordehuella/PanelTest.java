/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.lectordehuella;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author INTERNET
 */
public class PanelTest {

    private static int count;
    private static final List buttons = new ArrayList();
    // private JButton button = new JButton();

    public PanelTest() {
        JFrame frame = new JFrame();
        JButton button = new JButton();
        buttons.add(button);
        button.addActionListener((ActionEvent e) -> {
            count++;
            Iterator it = buttons.iterator();
            while (it.hasNext()) {
                ((JButton) it.next()).setText("clicks = " + count);
            }
        });
        button.setText("clicks = " + count);
        frame.getContentPane().add(button);
        frame.setSize(300, 300);
        frame.setLocation(400, 300);
        frame.setVisible(true);
        // Importante. Si se hace un exit se cerrar? el loader
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    public static void main(String[] args) {
        System.out.println("[Main] main class executed");
        PanelTest prueba = new PanelTest();
    }

}
