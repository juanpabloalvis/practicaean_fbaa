/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.servlets;

import com.digitalpersona.onetouch.DPFPDataPurpose;
import com.digitalpersona.onetouch.DPFPFeatureSet;
import com.digitalpersona.onetouch.DPFPGlobal;
import com.digitalpersona.onetouch.DPFPSample;
import com.digitalpersona.onetouch.DPFPTemplate;
import com.digitalpersona.onetouch.processing.DPFPFeatureExtraction;
import com.digitalpersona.onetouch.processing.DPFPImageQualityException;
import com.digitalpersona.onetouch.verification.DPFPVerification;
import com.digitalpersona.onetouch.verification.DPFPVerificationResult;
import com.fbaa.lectordehuella.JDBCFBAALectorHuella;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author INTERNET
 */
public class FingerPrintServlet extends HttpServlet {

    public final int TIPO_ACCION_SERVLET_REGISTRO = 1;
    public final int TIPO_ACCION_SERVLET_VERIFICACION = 2;
    ArrayList<IdAndHuella> listaHuellas = new ArrayList<>();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // response.setContentType("text/html;charset=UTF-8");
       
        String mensajeRespuesta = "Respuesta exitosa";
        // get the input stream from the applet
        InputStream in = request.getInputStream();
        // create an object input stream

        try {

            String cedula = request.getParameter("cedula");

            int tipo_operacion = 0;
            try {
                tipo_operacion = Integer.parseInt(request.getParameter("tipo"));

            } catch (NumberFormatException e) {
                mensajeRespuesta = "Error comvirtiendo el tipo de dato";
            }

            if (tipo_operacion == TIPO_ACCION_SERVLET_REGISTRO) {

                ObjectInputStream ois = new ObjectInputStream(in);
                DPFPTemplate templateFromClient;
                templateFromClient = DPFPGlobal.getTemplateFactory().createTemplate((byte[]) ois.readObject());
                mensajeRespuesta = JDBCFBAALectorHuella.ingresarPlantilla(cedula, templateFromClient);

            } else if (tipo_operacion == TIPO_ACCION_SERVLET_VERIFICACION) {
                ObjectInputStream ois = new ObjectInputStream(in);
                DPFPSample sample;
                sample = DPFPGlobal.getSampleFactory().createSample((byte[]) ois.readObject());

                DPFPFeatureExtraction featureExtractor = DPFPGlobal.getFeatureExtractionFactory().createFeatureExtraction();
                DPFPFeatureSet featureSet = featureExtractor.createFeatureSet(sample, DPFPDataPurpose.DATA_PURPOSE_VERIFICATION);

                DPFPVerification matcher = DPFPGlobal.getVerificationFactory().createVerification();
                matcher.setFARRequested(DPFPVerification.MEDIUM_SECURITY_FAR);

                JDBCFBAALectorHuella jdbcfbaaLectorHuella = new JDBCFBAALectorHuella();
                listaHuellas = jdbcfbaaLectorHuella.getListaHuellas();

                DPFPVerificationResult result;
                mensajeRespuesta = "No se encontro la huella en el servidor, por favor asegurese que esté registrado.";
                for (IdAndHuella idHuella : listaHuellas) {
                    result = null;

                    result = matcher.verify(featureSet, idHuella.plantilla);
                    if (result.isVerified()) {
                        mensajeRespuesta = "Huella encontrada, cedula: \"" + idHuella.cedula + "\" " + (double) result.getFalseAcceptRate() / DPFPVerification.PROBABILITY_ONE;
                        mensajeRespuesta = JDBCFBAALectorHuella.registrarHora(idHuella.cedula);
                    }

                }

            }

        } catch (ClassNotFoundException | DPFPImageQualityException ex) {
            mensajeRespuesta = "Ocurrio un error en el servidor de registro e identificacion de huella";
        }

       /* TODO output your page here. You may use following sample code. */

        ObjectOutputStream out = new ObjectOutputStream(response.getOutputStream());
        out.writeObject(mensajeRespuesta);
        out.flush();

        in.close();

        out.close();

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
