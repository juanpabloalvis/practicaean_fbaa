/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.servlets;

import com.digitalpersona.onetouch.DPFPTemplate;

/**
 *
 * @author INTERNET
 */
public class IdAndHuella {

    String cedula;
    DPFPTemplate plantilla;

    public IdAndHuella() {
    }

    public IdAndHuella(String cedula, DPFPTemplate plantilla) {
        this.cedula = cedula;
        this.plantilla = plantilla;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public DPFPTemplate getPlantilla() {
        return plantilla;
    }

    public void setPlantilla(DPFPTemplate plantilla) {
        this.plantilla = plantilla;
    }

}
