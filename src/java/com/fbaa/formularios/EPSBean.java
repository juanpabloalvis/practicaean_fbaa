/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.DAO.GestionarEpsDAO;
import com.fbaa.jpa.entities.EPS;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "epsBean")
@SessionScoped
public class EPSBean implements Serializable{

    private EPS eps;
    private String nombreEPSSeleccionada;
    private String msg;
    private final GestionarEpsDAO epsDAO = new GestionarEpsDAO();

    @PostConstruct
    public void init() {
        eps = new EPS();
    }

    public String crearEPS() {
        if (this.eps.getNombreEPS() != null) {
            epsDAO.crearEPS(this.eps);
        }
        return null;
    }

    public String obtenerEPS() {
        if (eps.getId() != null) {
            EPS epsFromDao;
            epsFromDao = epsDAO.obtenerEPSByID(eps.getId());
            this.eps = epsFromDao;
        }
        return null;
    }

    public String actualizarEPS() {

        if (this.eps.getId() != null) {
            epsDAO.actualizarEPS(this.eps);
            limpiarTodo();
            this.msg = "EPS actualizada exitosamente!";
        } else {
            this.msg = "Por favor seleccione una EPS!";
        }
        return null;
    }

    public String eliminarEPS() {
        if (this.eps.getId() != null) {
            epsDAO.eliminarEPS(this.eps);
            limpiarTodo();
            this.msg = "EPS eliminada exitosamente!";
        } else {
            this.msg = "Por favor seleccione una EPS!";
        }
        return null;
    }

    public ArrayList<EPS> getEPSsList() {
        return epsDAO.obtenerEPSs();
    }

    private void limpiarTodo() {
        this.eps.setId(null);
        this.eps.setNombreEPS("");
    }

    public void validarCrearEps(FacesContext fc, UIComponent c, Object valor) {
        UIInput nombreEps = (UIInput) c.findComponent("nombreEps");
        String vEps = (String) nombreEps.getLocalValue();
        if (vEps != null && vEps.length() > 40) {
            throw new ValidatorException(new FacesMessage("Longitud excedida"));
        }
    }

    public void validarActualizarEps(FacesContext fc, UIComponent c, Object valor) {
        UIInput uid = (UIInput) c.findComponent("uid");
        UIInput uname = (UIInput) c.findComponent("uname");

    }

    public EPS getEPS() {
        return eps;
    }

    public void setEPS(EPS eps) {
        this.eps = eps;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNombreEPSSeleccionada() {
        return nombreEPSSeleccionada;
    }

    public void setNombreEPSSeleccionada(String nombreEPSSeleccionada) {
        this.nombreEPSSeleccionada = nombreEPSSeleccionada;
    }

    /**
     * Creates a new instance of EPSBean
     */
    public EPSBean() {
    }

}
