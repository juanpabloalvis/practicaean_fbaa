/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "utilBean")
@ApplicationScoped
public class UtilBean implements Serializable{

    public String getToday() {
        DateFormat dateFormat = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
