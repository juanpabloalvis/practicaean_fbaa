/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.entities.AreaVoluntariado;
import com.fbaa.jpa.DAO.GestionarVoluntarioDAO;
import com.fbaa.jpa.entities.Universidad;
import com.fbaa.jpa.entities.Voluntario;
import com.fbaa.util.ResourcesUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Juan Pablo Alvis <juanpabloalvis@gmail.com>
 */
@ManagedBean(name = "busquedaVoluntario")
@SessionScoped
public class BusquedaVoluntarioBean implements Serializable {

    private Long cedulaBusqueda;
    private static final long serialVersionUID = 1L;
    private Voluntario voluntario = new Voluntario();
    private String titulo;
    private final GestionarVoluntarioDAO mvol = new GestionarVoluntarioDAO();

    private List<ConceptoCertificacion> results = new ArrayList<>();

    private String valPlan;
    private String valPunt;
    private String valRelaci;
    private String valCumpli;
    private String valLogro;
    // private ArrayList<ConceptoCertificacion> conceptoPlan = new ArrayList<>();
    private final static Map<String, Object> conpPlan;
    private static Map<String, Object> conpPunt;
    private static Map<String, Object> conpRelaci;
    private static Map<String, Object> conpCumpli;
    private static Map<String, Object> conpLogro;

    static {
        conpPlan = new LinkedHashMap<>();
        conpPlan.put("Deficiente", 1);
        conpPlan.put("Insatisfactorio", 2);
        conpPlan.put("Regular", 3);
        conpPlan.put("Satisfactorio", 4);
        conpPlan.put("Sobresaliente", 5);

        conpPunt = conpPlan;
        conpRelaci = conpPlan;
        conpCumpli = conpPlan;
        conpLogro = conpPlan;
    }

    String[] diasSemana;
    private String[] diasSeleccionados;

    public BusquedaVoluntarioBean() {
    }

    public String[] getDiasSeleccionados() {
        if (voluntario != null && voluntario.getDias() !=null) {
            return calcularBinToDiasSemana(voluntario.getDias());
        } else {
            return diasSeleccionados;
        }
    }

    public void setDiasSeleccionados(String[] diasSeleccionados) {
        this.diasSeleccionados = diasSeleccionados;
    }

    public String[] getDiasSemanaValores() {
        diasSemana = new String[7];
        diasSemana[0] = "Lunes";
        diasSemana[1] = "Martes";
        diasSemana[2] = "Miercoles";
        diasSemana[3] = "Jueves";
        diasSemana[4] = "Viernes";
        diasSemana[5] = "Sabado";
        diasSemana[6] = "Domingo";

        return diasSemana;
    }


    public void calcularDiasSemanaToBin() {
        StringBuilder dias = new StringBuilder("00000000");
        // {1,4,7} -->01001010
        //int i = 0;
        for (String diasSemana1 : diasSeleccionados) {
            dias.setCharAt(Arrays.asList(getDiasSemanaValores()).indexOf(diasSemana1), '1');
        }
        voluntario.setDias(dias.toString());
    }

    public String[] calcularBinToDiasSemana(String dd) {
        String[] cadena = new String[7];
        List<String> scripts = new ArrayList<>();
        for (int i = 0; i < dd.length(); i++) {
            if (dd.charAt(i) == '1') {
                scripts.add(getDiasSemanaValores()[i] + "");
            }
        }
        cadena = scripts.toArray(new String[scripts.size()]);
        return cadena;
    }
    //fin valores semans

    public List<ConceptoCertificacion> getResults() {
        return results;
    }

    public void setResults(List<ConceptoCertificacion> results) {
        this.results = results;
    }

    public Map<String, Object> getConpPlan() {
        return conpPlan;
    }

//    public void setConpPlan(Map<String, Object> conpPlan) {
//        this.conpPlan = conpPlan;
//    }
    public Map<String, Object> getConpPunt() {
        return conpPunt;
    }

    public void setConpPunt(Map<String, Object> conpPunt) {
        this.conpPunt = conpPunt;
    }

    public Map<String, Object> getConpRelaci() {
        return conpRelaci;
    }

    public void setConpRelaci(Map<String, Object> conpRelaci) {
        this.conpRelaci = conpRelaci;
    }

    public Map<String, Object> getConpCumpli() {
        return conpCumpli;
    }

    public void setConpCumpli(Map<String, Object> conpCumpli) {
        this.conpCumpli = conpCumpli;
    }

    public Map<String, Object> getConpLogro() {
        return conpLogro;
    }

    public void setConpLogro(Map<String, Object> conpLogro) {
        this.conpLogro = conpLogro;
    }

    public String getValPlan() {
        return valPlan;
    }

    public void setValPlan(String valPlan) {
        this.valPlan = valPlan;
    }

    public String getValPunt() {
        return valPunt;
    }

    public void setValPunt(String valPunt) {
        this.valPunt = valPunt;
    }

    public String getValRelaci() {
        return valRelaci;
    }

    public void setValRelaci(String valRelaci) {
        this.valRelaci = valRelaci;
    }

    public String getValCumpli() {
        return valCumpli;
    }

    public void setValCumpli(String valCumpli) {
        this.valCumpli = valCumpli;
    }

    public String getValLogro() {
        return valLogro;
    }

    public void setValLogro(String valLogro) {
        this.valLogro = valLogro;
    }

    public void updateUniversidad() {
        voluntario.setUniversidad(mvol.obtenerUniversidades().get(0));
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Voluntario getVoluntario() {
        return voluntario;
    }

    public void setVoluntario(Voluntario voluntario) {
        this.voluntario = voluntario;
    }

    @PostConstruct
    public void init() {
        voluntario = new Voluntario();
    }

    public String addAction() {
        mvol.guardarVoluntario(this.voluntario);
        this.setTitulo(ResourcesUtil.getString("#{msg['ingrear_nuevo_voluntario']}"));
        return null;
    }

    public String seleccionar(Voluntario voluntario) {
        this.setVoluntario(voluntario);
        return null;
    }

    public String limpiarFormulario() {
        this.setCedulaBusqueda(0L);
        Universidad universidad = new Universidad();
        AreaVoluntariado areaVoluntariado = new AreaVoluntariado();
        voluntario = new Voluntario();
        voluntario.setAreaVoluntariado(areaVoluntariado);
        voluntario.setUniversidad(universidad);
        return null;
    }

    public String deleteAction(Voluntario voluntario) {
        mvol.eliminarVoluntario(voluntario);
        voluntario = new Voluntario();
        return null;
    }

    public String updateAction(Voluntario voluntario) {
        calcularDiasSemanaToBin();
        mvol.actualizarVoluntario(voluntario);
        voluntario = new Voluntario();
        return null;
    }

    public Long getCedulaBusqueda() {
        return cedulaBusqueda;
    }

    public void setCedulaBusqueda(Long cedulaBusqueda) {
        this.cedulaBusqueda = cedulaBusqueda;
    }

    public void findVoluntarioByID() {
        if (cedulaBusqueda != null && cedulaBusqueda != 0) {
            voluntario = mvol.obtenerVoluntarioPorDocumentoIdentidad(cedulaBusqueda);
            if (voluntario != null) {
                calcularBinToDiasSemana(voluntario.getDias());
            }
        }
    }

    private static class ConceptoCertificacion {

        private String conceptoNombre;
        private List<ValorCalificacion> valorCalificacion;

        public ConceptoCertificacion() {
        }

        public ConceptoCertificacion(String conceptoNombre, List<ValorCalificacion> valorCalificacion) {
            this.conceptoNombre = conceptoNombre;
            this.valorCalificacion = valorCalificacion;
        }

        public String getConceptoNombre() {
            return conceptoNombre;
        }

        public void setConceptoNombre(String conceptoNombre) {
            this.conceptoNombre = conceptoNombre;
        }

        public List<ValorCalificacion> getValorCalificacion() {
            return valorCalificacion;
        }

        public void setValorCalificacion(List<ValorCalificacion> valorCalificacion) {
            this.valorCalificacion = valorCalificacion;
        }

    }

    private static class ValorCalificacion {

        public int idCalificacion;
        public int valorCalificacion;

        public ValorCalificacion() {
        }

        public int getIdCalificacion() {
            return idCalificacion;
        }

        public void setIdCalificacion(int idCalificacion) {
            this.idCalificacion = idCalificacion;
        }

        public int getValorCalificacion() {
            return valorCalificacion;
        }

        public void setValorCalificacion(int valorCalificacion) {
            this.valorCalificacion = valorCalificacion;
        }

    }

}
