/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.DAO.GestionarVoluntarioDAO;
import com.fbaa.jpa.entities.Voluntario;
import com.fbaa.util.ResourcesUtil;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Juan Pablo Alvis <juanpabloalvis@gmail.com>
 */
@ManagedBean(name = "voluntario_Bk")
@SessionScoped
public class VoluntariadoBean_20150410 implements Serializable {

    private static final long serialVersionUID = 1L;
    private Voluntario vol;
    private String titulo;
    private final GestionarVoluntarioDAO mvol = new GestionarVoluntarioDAO();

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Voluntario getVol() {
        return vol;
    }

    public void setVol(Voluntario vol) {
        this.vol = vol;
    }

    @PostConstruct
    public void init() {
        vol = new Voluntario();
    }

    public ArrayList<Voluntario> getVoluntariosList() {
        return mvol.obtenerVoluntarios();
    }

    public String addAction() {
        mvol.guardarVoluntario(this.vol);
        this.setTitulo(ResourcesUtil.getString("#{msg['ingrear_nuevo_voluntario']}"));
        return null;
    }

    public String seleccionar(Voluntario voluntario) {
        setVol(voluntario);
        this.setTitulo(ResourcesUtil.getString("#{msg['modificar_voluntario']}"));
        return null;
    }

    public String limpiarFormulario() {
        vol = new Voluntario();
        return null;
    }

    public String deleteAction(Voluntario voluntario) {
        mvol.eliminarVoluntario(voluntario);
        this.setTitulo(ResourcesUtil.getString("#{msg['ingrear_nuevo_voluntario']}"));
        vol = new Voluntario();
        return null;
    }
}
