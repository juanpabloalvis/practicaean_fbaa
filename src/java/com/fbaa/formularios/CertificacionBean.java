/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.util.ReporteVoluntarioJasper;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "certifiacionBean")
@SessionScoped
public class CertificacionBean implements Serializable {

    private DateFormat dateFormat;
    Date date;
    
    JasperPrint jasperPrint;
    private ReporteVoluntarioJasper usuarioReporte = new ReporteVoluntarioJasper();
    //HttpServletResponse httpServletResponse;
    @ManagedProperty(value = "#{busquedaVoluntario}")
    private BusquedaVoluntarioBean busquedaVoluntario;

    private List<ReporteVoluntarioJasper> listOfUser;

    public ReporteVoluntarioJasper getUsuarioReporte() {
        return usuarioReporte;
    }

    public void setUsuarioReporte(ReporteVoluntarioJasper usuarioReporte) {
        this.usuarioReporte = usuarioReporte;
    }

    public void init() throws JRException {

        dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
        date = new Date();
        dateFormat.format(date);

        listOfUser = new ArrayList<>();
        usuarioReporte.setCedula(String.valueOf(busquedaVoluntario.getVoluntario().getCedula()));
        usuarioReporte.setNombre(busquedaVoluntario.getVoluntario().getNombres());
        usuarioReporte.setApellido(busquedaVoluntario.getVoluntario().getApellidos());
        usuarioReporte.setHoras(busquedaVoluntario.getVoluntario().getIntensidadHoras());
        usuarioReporte.setValPlan(busquedaVoluntario.getValPlan());
        usuarioReporte.setValPunt(busquedaVoluntario.getValPunt());
        usuarioReporte.setValRelaci(busquedaVoluntario.getValRelaci());
        usuarioReporte.setValCumpli(busquedaVoluntario.getValCumpli());
        usuarioReporte.setValLogro(busquedaVoluntario.getValLogro());

        double subtotal = ((Double.valueOf(busquedaVoluntario.getValPlan())
                + Double.valueOf(busquedaVoluntario.getValPunt())
                + Double.valueOf(usuarioReporte.getValRelaci())
                + Double.valueOf(usuarioReporte.getValCumpli())
                + Double.valueOf(usuarioReporte.getValLogro()))
                / 5);
        DecimalFormat formateador = new DecimalFormat("########.###");

        usuarioReporte.setValTotal(String.valueOf(formateador.format(subtotal)));

        listOfUser.add(usuarioReporte);

        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(listOfUser, false);
        String reportPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/jasperreports/report.jasper");
        jasperPrint = JasperFillManager.fillReport(reportPath, new HashMap(), beanCollectionDataSource);
    }

    public void PDF() throws JRException, IOException {
        init();
        HttpServletResponse httpServletResponse;
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + dateFormat.format(date) + "_ReporteVoluntario_"+busquedaVoluntario.getVoluntario().getNombres()+".pdf");

        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
        FacesContext.getCurrentInstance().responseComplete();

    }

    public void DOCX() throws JRException, IOException {
        init();
        HttpServletResponse httpServletResponse;
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + dateFormat.format(date) + "_ReporteVoluntario_"+busquedaVoluntario.getVoluntario().getNombres()+".docx");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JRDocxExporter docxExporter = new JRDocxExporter();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        docxExporter.setParameter(JRDocxExporterParameter.OUTPUT_STREAM, servletOutputStream);
        docxExporter.exportReport();
    }

    public void XLSX() throws JRException, IOException {
        init();
        HttpServletResponse httpServletResponse;
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=report.xlsx");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JRXlsxExporter docxExporter = new JRXlsxExporter();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        docxExporter.exportReport();
    }

    public void ODT() throws JRException, IOException {
        init();
        HttpServletResponse httpServletResponse;
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=report.odt");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JROdtExporter docxExporter = new JROdtExporter();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        docxExporter.exportReport();
    }

    public void PPT() throws JRException, IOException {
        init();
        HttpServletResponse httpServletResponse;
        httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=report.pptx");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JRPptxExporter docxExporter = new JRPptxExporter();
        docxExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        docxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
        docxExporter.exportReport();
    }

//    @PostConstruct
//    public void init() {
//    }
    /**
     * Creates a new instance of EPSBean
     */
    public CertificacionBean() {
    }

    public BusquedaVoluntarioBean getBusquedaVoluntario() {
        return busquedaVoluntario;
    }

    public void setBusquedaVoluntario(BusquedaVoluntarioBean busquedaVoluntario) {
        this.busquedaVoluntario = busquedaVoluntario;
    }

}
