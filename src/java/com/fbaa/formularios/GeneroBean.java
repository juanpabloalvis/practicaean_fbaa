/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.DAO.GestionarGeneroDAO;
import com.fbaa.jpa.entities.AreaVoluntariado;
import com.fbaa.jpa.entities.Genero;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "generoBean")
@ViewScoped
public class GeneroBean implements Serializable {

    private Genero genero;
    private String nombreGeneroSeleccionado;
    private String msg;
    private final GestionarGeneroDAO generosVoluntariadoDAO = new GestionarGeneroDAO();

    @PostConstruct
    public void init() {
        genero = new Genero();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getGeneroSeleccionado() {
        return nombreGeneroSeleccionado;
    }

    public void setNombreGeneroSeleccionado(String nomGeneroSeleccionado) {
        this.nombreGeneroSeleccionado = nomGeneroSeleccionado;
    }

    public String obtenerGenero() {
        if (genero.getId() != null) {
            Genero generoVolFromDao;
            generoVolFromDao = generosVoluntariadoDAO.obtenerGeneroByID(genero.getId());
            this.genero = generoVolFromDao;
        }
        return null;
    }

    private void limpiarTodo() {
        this.genero.setId(null);
        this.genero.setNombreGenero("");
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public String getNombreGeneroSeleccionado() {
        return nombreGeneroSeleccionado;
    }

    public ArrayList<Genero> getGenerosList() {
        return generosVoluntariadoDAO.obtenerGeneros();
    }

    /**
     * Creates a new instance of AreaVoluntariadoBean
     */
    public GeneroBean() {
    }

}
