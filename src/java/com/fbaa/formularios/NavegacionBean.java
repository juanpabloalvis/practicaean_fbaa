/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "navegacionBean")
@SessionScoped
public class NavegacionBean implements Serializable {

    private static final long serialVersionUID = 3L;

    /**
     * Redirect to login page.
     *
     * @return Login page name.
     */
    public String redirectToLogin() {
        return "login.xhtml?faces-redirect=true";
    }

    /**
     * Go to login page.
     *
     * @return Login page name.
     */
    public String toLogin() {
        return "login.xhtml";
    }

    /**
     * Redirect to info page.
     *
     * @return Info page name.
     */
    public String redirectToInfo() {
        return "/info.xhtml?faces-redirect=true";
    }

    /**
     * Go to info page.
     *
     * @return Info page name.
     */
    public String toInfo() {
        return "/info.xhtml";
    }

    /**
     * Redirect to welcome page.
     *
     * @return Welcome page name.
     */
    public String redirectToIndex() {
        return "/index.xhtml?faces-redirect=true";
    }

    /**
     * Go to welcome page.
     *
     * @return Welcome page name.
     */
    public String toIndex() {
        return "/index.xhtml";
    }

    /**
     * Redirect to voluntarios page.
     *
     * @return Voluntarios page name.
     */
    public String redirectToFormatoVoluntariado() {
        return "/FormatoVoluntariado.xhtml?faces-redirect=true";
    }

    /**
     * Go to welcome page.
     *
     * @return Welcome page name.
     */
    public String toFormatoVoluntariado() {
        return "/FormatoVoluntariado.xhtml";
    }

    /**
     * Redirect to FormatoEstudiantes page.
     *
     * @return FormatoEstudiantes page name.
     */
    public String redirectToFormatoEstudiantes() {
        return "/secured/FormatoEstudiantes.xhtml?faces-redirect=true";
    }

    /**
     * Go to FormatoEstudiantes page.
     *
     * @return FormatoEstudiantes page name.
     */
    public String toFormatoEstudiantes() {
        return "/secured/FormatoEstudiantes.xhtml";
    }

    /**
     * Redirect to BuscarVoluntario page.
     *
     * @return BuscarVoluntario page name.
     */
    public String redirectToBuscarVoluntario() {
        return "/secured/BuscarVoluntario.xhtml?faces-redirect=true";
    }

    /**
     * Go to BuscarVoluntario page.
     *
     * @return BuscarVoluntario page name.
     */
    public String toFormatoBuscarVolunatario() {
        return "/secured/BuscarVoluntario.xhtml";
    }

    /**
     * Redirect to Universidad page.
     *
     * @return Universidad page name.
     */
    public String redirectToUniversidad() {
        return "/secured/Universidad.xhtml?faces-redirect=true";
    }

    /**
     * Go to Universidad page.
     *
     * @return Universidad page name.
     */
    public String toUniversidad() {
        return "/secured/Universidad.xhtml";
    }

    /**
     * Redirect to AreaVoluntariado page.
     *
     * @return AreaVoluntariado page name.
     */
    public String redirectToAreaVoluntariado() {
        return "/secured/AreaVoluntariado.xhtml?faces-redirect=true";
    }

    /**
     * Go to AreaVoluntariado page.
     *
     * @return AreaVoluntariado page name.
     */
    public String toAreaVoluntariado() {
        return "/secured/AreaVoluntariado.xhtml";
    }

    /**
     * Redirect to EPS page.
     *
     * @return EPS page name.
     */
    public String redirectToEPS() {
        return "/secured/EPS.xhtml?faces-redirect=true";
    }

    /**
     * Go to EPS page.
     *
     * @return EPS page name.
     */
    public String toEPS() {
        return "/secured/EPS.xhtml";
    }

    /**
     * Redirect to Voluntario page.
     *
     * @return Voluntario page name.
     */
    public String redirectToVoluntario() {
        return "/secured/Voluntario.xhtml?faces-redirect=true";
    }

    /**
     * Go to Voluntario page.
     *
     * @return Voluntario page name.
     */
    public String toVoluntario() {
        return "/secured/Voluntario.xhtml";
    }

    /**
     * Redirect to GenerarCertificacionVoluntario page.
     *
     * @return GenerarCertificacionVoluntario page name.
     */
    public String redirectGenerarCertificacionVoluntario() {
        return "/GenerarCertificacionVoluntario.xhtml?faces-redirect=true";
    }

    /**
     * Go to GenerarCertificacionVoluntario page.
     *
     * @return GenerarCertificacionVoluntario page name.
     */
    public String toGenerarCertificacionVoluntario() {
        return "/secured/GenerarCertificacionVoluntario.xhtml";
    }
    /**
     * Redirect to HistorialHoras page.
     *
     * @return HistorialHoras page name.
     */
    public String redirectHistorialHoras() {
        return "/secured/HistorialHoras.xhtml?faces-redirect=true";
    }

    /**
     * Go to HistorialHoras page.
     *
     * @return HistorialHoras page name.
     */
    public String toHistorialHoras() {
        return "/HistorialHoras.xhtml";
    }

}
