/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.DAO.GestionarVoluntarioDAO;
import com.fbaa.jpa.entities.AreaVoluntariado;
import com.fbaa.jpa.entities.Universidad;
import com.fbaa.jpa.entities.Voluntario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "voluntarioBean")
@SessionScoped
public class VoluntarioBean implements Serializable{

    private Voluntario voluntario;
    private String[] diasSemana;

    private String msg;
    private final GestionarVoluntarioDAO epsDAO = new GestionarVoluntarioDAO();

    //gestionarVoluntarioDAO
    @PostConstruct
    public void init() {
        voluntario = new Voluntario();
//        AreaVoluntariado areaVoluntariado = new AreaVoluntariado();
//        areaVoluntariado.setId(1L);
//        voluntario.setAreaVoluntariado(areaVoluntariado);
//        Universidad universidad = new Universidad();
//        areaVoluntariado.setId(1L);
//        voluntario.setUniversidad(universidad);
//        EPS eps = new EPS();
//        eps.setId(1L);
//        voluntario.setEPS(eps);
//        Genero genero = new Genero();
//        genero.setId(1L);
//        voluntario.setGenero(genero);
    }

    public String[] getDiasSemanaValores() {
        diasSemana = new String[7];
        diasSemana[0] = "Lunes";
        diasSemana[1] = "Martes";
        diasSemana[2] = "Miercoles";
        diasSemana[3] = "Jueves";
        diasSemana[4] = "Viernes";
        diasSemana[5] = "Sabado";
        diasSemana[6] = "Domingo";

        return diasSemana;
    }

    public String[] getDiasSemana() {
        return diasSemana;
    }

    public void setDiasSemana(String[] diasSemana) {
        this.diasSemana = diasSemana;
    }

    public void calcularDiasSemanaToBin() {
        StringBuilder dias = new StringBuilder("00000000");
        // {1,4,7} -->01001010
        //int i = 0;
        for (String diasSemana1 : getDiasSemana()) {
            dias.setCharAt(Arrays.asList(getDiasSemanaValores()).indexOf(diasSemana1), '1');
        }
        voluntario.setDias(dias.toString());
    }

    public void calcularBinToDiasSemana() {
        String dias = voluntario.getDias();
        List<String> scripts = new ArrayList<>();
        for (int i = 0; i < dias.length(); i++) {
            System.out.println("-->" + diasSemana.length);
            if (dias.charAt(i) == '1') {
                scripts.add(i + "");
            }
            diasSemana = scripts.toArray(new String[scripts.size()]);
        }
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String crearVoluntario() {
        if (this.voluntario.getCedula() != 0) {
            calcularDiasSemanaToBin();
            epsDAO.guardarVoluntario(this.voluntario);
        }
        voluntario = new Voluntario();
        return null;
    }

    public String obtenerVoluntario() {
        if (voluntario.getId() != null) {
            Voluntario voluntarioFromDao;
            voluntarioFromDao = epsDAO.obtenerVoluntario(voluntario.getId());
            this.voluntario = voluntarioFromDao;
        }
        return null;
    }

    public String obtenerVoluntarioByCedula() {
        if (voluntario.getId() != null) {
            Voluntario voluntarioFromDao;
            voluntarioFromDao = epsDAO.obtenerVoluntarioPorDocumentoIdentidad(voluntario.getCedula());
            this.voluntario = voluntarioFromDao;
        }
        return null;
    }

    public String actualizarVoluntario() {

        if (this.voluntario.getId() != null) {
            epsDAO.actualizarVoluntario(this.voluntario);
            limpiarTodo();
            this.msg = "Voluntario actualizado exitosamente!";
        } else {
            this.msg = "Por favor seleccione un Voluntario!";
        }
        return null;
    }

    public String eliminarVoluntario() {
        if (this.voluntario.getId() != null) {
            epsDAO.eliminarVoluntario(this.voluntario);
            limpiarTodo();
            this.msg = "Voluntario eliminado exitosamente!";
        } else {
            this.msg = "Por favor seleccione un voluntario!";
        }
        return null;
    }

    public ArrayList<Voluntario> getVoluntariosList() {
        return epsDAO.obtenerVoluntarios();
    }

    private void limpiarTodo() {
        this.voluntario.setId(0L);
        this.voluntario.setCedula(0L);
        this.voluntario.setApellidos("");
        this.voluntario.setNombres("");
    }

    public void limpiarFormulario() {
        this.voluntario.setId(0L);
        this.voluntario.setCedula(0L);
        this.voluntario.setApellidos("");
        this.voluntario.setNombres("");
        Universidad universidad = new Universidad();
        AreaVoluntariado areaVoluntariado = new AreaVoluntariado();
        // this.voluntario = new Voluntario();
        this.voluntario.setAreaVoluntariado(areaVoluntariado);
        this.voluntario.setUniversidad(universidad);

    }

    public Voluntario getVoluntario() {
        return voluntario;
    }

    public void setVoluntario(Voluntario voluntarioP) {
        this.voluntario = voluntarioP;
    }

    /**
     * Creates a new instance of EPSBean
     */
    public VoluntarioBean() {
    }

}
