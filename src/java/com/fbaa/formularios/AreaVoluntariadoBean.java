/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.DAO.GestionarAreaVoluntariadoDAO;
import com.fbaa.jpa.entities.AreaVoluntariado;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "areaVoluntariadoBean")
@ViewScoped
public class AreaVoluntariadoBean implements Serializable{

    private AreaVoluntariado areaVoluntariado;
    private String nombreAreaVoluntariadoSeleccionada;
    private String msg;
    private final GestionarAreaVoluntariadoDAO areaVoluntariadoDAO = new GestionarAreaVoluntariadoDAO();

    @PostConstruct
    public void init() {
        areaVoluntariado = new AreaVoluntariado();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNombreAreaVoluntariadoSeleccionada() {
        return nombreAreaVoluntariadoSeleccionada;
    }

    public void setNombreAreaVoluntariadoSeleccionada(String nombreAreaVolSeleccionada) {
        this.nombreAreaVoluntariadoSeleccionada = nombreAreaVolSeleccionada;
    }

    public String crearAreaVoluntariado() {
        if (this.areaVoluntariado.getNombreAreaVoluntariado() != null) {
            areaVoluntariadoDAO.crearAreaVoluntariado(this.areaVoluntariado);
        }
        return null;
    }

    public String obtenerAreaVoluntariado() {
        if (areaVoluntariado.getId() != null) {
            AreaVoluntariado areavolFromDao;
            areavolFromDao = areaVoluntariadoDAO.obtenerAreaVoluntariadoByID(areaVoluntariado.getId());
            this.areaVoluntariado = areavolFromDao;
        }
        return null;
    }

    public String actualizarAreaVoluntariado() {

        if (this.areaVoluntariado.getId() != null) {
            areaVoluntariadoDAO.actualizarAreaVoluntariado(this.areaVoluntariado);
            limpiarTodo();
            this.msg = "Area de voluntariado actualizada exitosamente!";
        } else {
            this.msg = "Por favor seleccione una area de voluntariado!";
        }
        return null;
    }

    public String eliminarAreaVoluntariado() {
        if (this.areaVoluntariado.getId() != null) {
            areaVoluntariadoDAO.eliminarAreaVoluntariado(this.areaVoluntariado);
            limpiarTodo();
            this.msg = "Area de voluntariado eliminada exitosamente!";
        } else {
            this.msg = "Por favor seleccione una area de voluntariado!";
        }
        return null;
    }

    public ArrayList<AreaVoluntariado> getAreasVoluntariadoList() {
        return areaVoluntariadoDAO.obtenerAreasVoluntariado();
    }

    private void limpiarTodo() {
        this.areaVoluntariado.setId(null);
        this.areaVoluntariado.setNombreAreaVoluntariado("");
    }

    public AreaVoluntariado getAreaVoluntariado() {
        return areaVoluntariado;
    }

    public void setAreaVoluntariado(AreaVoluntariado areaVol) {
        this.areaVoluntariado = areaVol;
    }

    /**
     * Creates a new instance of AreaVoluntariadoBean
     */
    public AreaVoluntariadoBean() {
    }

}
