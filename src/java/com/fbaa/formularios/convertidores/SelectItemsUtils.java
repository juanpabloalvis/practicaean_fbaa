/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios.convertidores;


import java.util.Iterator;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import org.jgroups.util.ArrayIterator;

/**
 *
 * @author INTERNET
 */
public final class SelectItemsUtils {

//     public static Object findValueByStringConversion(FacesContext context, UIComponent component, String value, Converter converter) {
//        return findValueByStringConversion(context, component, new SelectItemsIterator(context, component), value, converter);        
//    }
    private SelectItemsUtils() {}
 
     private static Object findValueByStringConversion(FacesContext context, UIComponent component, Iterator<SelectItem> items, String value, Converter converter) {
        while (items.hasNext()) {
            SelectItem item = items.next();
            if (item instanceof SelectItemGroup) {
                SelectItem subitems[] = ((SelectItemGroup) item).getSelectItems();
                if (!isEmpty(subitems)) {
                    Object object = findValueByStringConversion(context, component, new ArrayIterator(subitems), value, converter);
                    if (object != null) {
                        return object;
                    }
                }
            } else if (!item.isNoSelectionOption() && value.equals(converter.getAsString(context, component, item.getValue()))) {
                return item.getValue();
            }
        }        
        return null;
    }
     public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;    
    }
}
