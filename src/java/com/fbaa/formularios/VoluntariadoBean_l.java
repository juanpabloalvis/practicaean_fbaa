/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.entities.AreaVoluntariado;
import com.fbaa.jpa.DAO.GestionarVoluntarioDAO;
import com.fbaa.jpa.entities.Universidad;
import com.fbaa.jpa.entities.Voluntario;
import com.fbaa.util.ResourcesUtil;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Juan Pablo Alvis <juanpabloalvis@gmail.com>
 */
@ManagedBean(name = "voluntario")
@SessionScoped
public class VoluntariadoBean_l implements Serializable {

    private long cedulaBusqueda;
    private static final long serialVersionUID = 1L;
    private Voluntario vol;
    private String titulo;

    private final GestionarVoluntarioDAO dataAccessObject = new GestionarVoluntarioDAO();

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Voluntario getVol() {
        return vol;
    }

    public void setVol(Voluntario vol) {
        this.vol = vol;
    }

    @PostConstruct
    public void init() {
        vol = new Voluntario();
        AreaVoluntariado areaVoluntariado = new AreaVoluntariado();
        areaVoluntariado.setId(1L);
        vol.setAreaVoluntariado(areaVoluntariado);
    }

    public ArrayList<Voluntario> getVoluntariosList() {
        return dataAccessObject.obtenerVoluntarios();
    }

    public ArrayList<Universidad> getUniversidadesList() {
        return dataAccessObject.obtenerUniversidades();
    }

//    public ArrayList<Universidad> getUniversidadesList() {
//        return mvol.obtenerUniversidades();
//    }
//
//    public ArrayList<AreaVoluntariado> getAreaVoluntariadoList() {
//        return mvol.obtenerAreasVoluntariado();
//    }
    public String addAction() {
        dataAccessObject.guardarVoluntario(this.vol);
        this.setTitulo(ResourcesUtil.getString("#{msg['ingrear_nuevo_voluntario']}"));
        return null;
    }

    public void updateUniversidad() {
        
    }

    public String seleccionar(Voluntario voluntario) {
        this.setVol(voluntario);
        this.setTitulo(ResourcesUtil.getString("#{msg['modificar_voluntario']}"));
        return null;
    }

    public String limpiarFormulario() {
        Universidad universidad = new Universidad();
        AreaVoluntariado areaVoluntariado = new AreaVoluntariado();
        vol = new Voluntario();
        vol.setAreaVoluntariado(areaVoluntariado);
        vol.setUniversidad(universidad);
        return null;
    }

    public String deleteAction(Voluntario voluntario) {
        dataAccessObject.eliminarVoluntario(voluntario);
        this.setTitulo(ResourcesUtil.getString("#{msg['ingrear_nuevo_voluntario']}"));
        //vol = new Voluntario();
        return null;
    }

    public long getCedulaBusqueda() {
        return cedulaBusqueda;
    }

    public void setCedulaBusqueda(long cedulaBusqueda) {
        this.cedulaBusqueda = cedulaBusqueda;
    }

}
