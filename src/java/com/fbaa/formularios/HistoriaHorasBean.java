/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.DAO.GestionarHistoriaHorasDAO;
import com.fbaa.jpa.entities.LinkHistorialHoras;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "historiaHorasBean")
@SessionScoped
public class HistoriaHorasBean implements Serializable {

    private String msg;
    private final GestionarHistoriaHorasDAO reporteHorasDAO = new GestionarHistoriaHorasDAO();
    private LinkHistorialHoras reporteHoras = null;
    @ManagedProperty(value = "#{busquedaVoluntario}")
    private BusquedaVoluntarioBean busquedaVoluntario;

    @PostConstruct
    public void init() {
        if (busquedaVoluntario.getCedulaBusqueda() != null) {
            
            reporteHoras = reporteHorasDAO.obtenerReporteHoras(busquedaVoluntario.getCedulaBusqueda());
        }
    }

    public ArrayList<LinkHistorialHoras.RegistroHoraDia> getReporteHorasList(Long id) {

         reporteHoras = reporteHorasDAO.obtenerReporteHoras(id);
        ArrayList<LinkHistorialHoras.RegistroHoraDia> registros = reporteHoras.getRegistrosHoras();

        return registros;
    }

    public LinkHistorialHoras getReporteHoras() {
        return reporteHoras;
    }

    public void setReporteHoras(LinkHistorialHoras reporteHoras) {
        this.reporteHoras = reporteHoras;
    }

    public BusquedaVoluntarioBean getBusquedaVoluntario() {
        return busquedaVoluntario;
    }

    public void setBusquedaVoluntario(BusquedaVoluntarioBean busquedaVoluntario) {
        this.busquedaVoluntario = busquedaVoluntario;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * Creates a new instance of EPSBean
     */
    public HistoriaHorasBean() {
    }

}
