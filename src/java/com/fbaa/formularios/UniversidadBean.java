/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.DAO.GestionarUniversidadDAO;
import com.fbaa.jpa.entities.Universidad;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author INTERNET
 */
@ManagedBean(name = "universidadBean")
@ViewScoped
public class UniversidadBean implements Serializable {

    private Universidad universidad;
    private String nombreUniversidadSeleccionada;
    private String msg;
    private final GestionarUniversidadDAO universidadesDAO = new GestionarUniversidadDAO();

    @PostConstruct
    public void init() {
        universidad = new Universidad();
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNombreUniversidadSeleccionada() {
        return nombreUniversidadSeleccionada;
    }

    public void setNombreUniversidadSeleccionada(String nombreUniversidadSeleccionada) {
        this.nombreUniversidadSeleccionada = nombreUniversidadSeleccionada;
    }

    public String crearUniversidad() {
        if (this.universidad.getNombreUniversidad() != null) {
            universidadesDAO.crearUniversidad(this.universidad);
        }
        return null;
    }

    public String obtenerUniversidad() {
        if (universidad.getId() != null) {
            Universidad univFromDAO;
            univFromDAO = universidadesDAO.obtenerUniversidadByID(universidad.getId());
            this.universidad = univFromDAO;
        }
        return null;
    }

    public String actualizarUniversidad() {

        if (this.universidad.getId() != null) {
            universidadesDAO.actualizarUniversidad(this.universidad);
            limpiarTodo();
            this.msg = "Universidad actualizada exitosamente!";
        } else {
            this.msg = "Por favor seleccione una universidad!";
        }
        return null;
    }

    public String eliminarUniversidad() {
        if (this.universidad.getId() != null) {

            universidadesDAO.eliminarUniversidad(this.universidad);
            limpiarTodo();
            this.msg = "Universidad eliminada exitosamente!";
        } else {
            this.msg = "Por favor seleccione una universidad!";
        }
        return null;
    }

    public ArrayList<Universidad> getUniversidadesList() {
        return universidadesDAO.obtenerUniversidades();
    }

    private void limpiarTodo() {
        this.universidad.setId(null);
        this.universidad.setNombreUniversidad("");
    }

    public Universidad getUniversidad() {
        return universidad;
    }

    public void setUniversidad(Universidad universidad) {
        this.universidad = universidad;
    }

    /**
     * Creates a new instance of UniversidadBean
     */
    public UniversidadBean() {
    }

}
