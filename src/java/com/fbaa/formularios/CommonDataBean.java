/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.formularios;

import com.fbaa.jpa.entities.AreaVoluntariado;
import com.fbaa.jpa.DAO.GestionarVoluntarioDAO;
import com.fbaa.jpa.entities.Universidad;
import com.fbaa.jpa.entities.Voluntario;
import java.io.Serializable;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Juan Pablo Alvis <juanpabloalvis@gmail.com>
 */
@ManagedBean(name = "commons")
@RequestScoped
public class CommonDataBean implements Serializable {

    public CommonDataBean() {
    }

    
    private static final long serialVersionUID = 6L;
    private String titulo;
    private final GestionarVoluntarioDAO mvol = new GestionarVoluntarioDAO();

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public ArrayList<Voluntario> getVoluntariosList() {
        return mvol.obtenerVoluntarios();
    }

    public ArrayList<Universidad> getUniversidadesList() {
        return mvol.obtenerUniversidades();
    }

    public ArrayList<AreaVoluntariado> getAreaVoluntariadoList() {
        return mvol.obtenerAreasVoluntariado();
    }

    public Voluntario getVoluntarioByDocumentoIdentidad(Long cedula) {
        return mvol.obtenerVoluntarioPorDocumentoIdentidad(cedula);
    }

}
