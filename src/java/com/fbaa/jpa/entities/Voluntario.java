/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author INTERNET
 */
@Entity
@Table(name = "TABLA_VOLUNTARIOS",
        uniqueConstraints = @UniqueConstraint(columnNames = {"cedula"}))
public class Voluntario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_VOLUNTARIO")
    private Long id;
    private long cedula;
    private String nombres;
    private String apellidos;
    private long telefono;
    private String correo;
    @OneToOne
    @JoinColumn(name = "id_genero")
    private Genero genero;
    @OneToOne
    @JoinColumn(name = "id_area_voluntariado")
    private AreaVoluntariado areaVoluntariado;
    private String nivelEstudios;
    @OneToOne
    @JoinColumn(name = "id_universidad")
    private Universidad universidad;
    private String carrera;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaIngreso;
    private int intensidadHoras;
    @Column(length = 8)
    private String dias;
    @OneToOne
    @JoinColumn(name = "id_eps")
    private EPS EPS;
    private String horarios;

    public Voluntario() {
    }

    public Voluntario(Long id, long cedula, String nombres, String apellidos, long telefono, String correo, Genero genero, AreaVoluntariado areaVoluntariado, String nivelEstudios, Universidad universidad, String carrera, Date fechaIngreso, int intensidadHoras, String dias, EPS EPS, String horarios) {
        this.id = id;
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.telefono = telefono;
        this.correo = correo;
        this.genero = genero;
        this.areaVoluntariado = areaVoluntariado;
        this.nivelEstudios = nivelEstudios;
        this.universidad = universidad;
        this.carrera = carrera;
        this.fechaIngreso = fechaIngreso;
        this.intensidadHoras = intensidadHoras;
        this.dias = dias;
        this.EPS = EPS;
        this.horarios = horarios;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Genero getGenero() {
        if (genero == null) {
            genero = new Genero();
        }
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public AreaVoluntariado getAreaVoluntariado() {
        if (areaVoluntariado == null) {
            areaVoluntariado = new AreaVoluntariado();
        }
        return areaVoluntariado;
    }

    public void setAreaVoluntariado(AreaVoluntariado areaVoluntariado) {
        this.areaVoluntariado = areaVoluntariado;
    }

    public String getNivelEstudios() {
        return nivelEstudios;
    }

    public void setNivelEstudios(String nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }

    public Universidad getUniversidad() {
        if (universidad == null) {
            universidad = new Universidad();
        }
        return universidad;
    }

    public void setUniversidad(Universidad universidad) {
        this.universidad = universidad;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getIntensidadHoras() {
        return intensidadHoras;
    }

    public void setIntensidadHoras(int intensidadHoras) {
        this.intensidadHoras = intensidadHoras;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    public String getHorarios() {
        return horarios;
    }

    public void setHorarios(String horarios) {
        this.horarios = horarios;
    }

    public EPS getEPS() {
        if (EPS == null) {
            EPS = new EPS();
        }
        return EPS;
    }

    public void setEPS(EPS EPS) {
        this.EPS = EPS;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Voluntario)) {
            return false;
        }
        Voluntario other = (Voluntario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.fbaa.jpa.Voluntario[ id=" + id + " ]";
    }

}
