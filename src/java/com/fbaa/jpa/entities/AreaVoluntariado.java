/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author INTERNET
 */
@Entity
@Table(name = "TABLA_AREA_VOLUNTARIADO")
public class AreaVoluntariado implements Serializable {

    @Id
    @GeneratedValue
    private Long id_area_voluntariado;
    private String nombreAreaVoluntariado;

    public String getNombreAreaVoluntariado() {
        return nombreAreaVoluntariado;
    }

    public void setNombreAreaVoluntariado(String nombreAreaVoluntariado) {
        this.nombreAreaVoluntariado = nombreAreaVoluntariado;
    }

    public Long getId() {
        return id_area_voluntariado;
    }

    public void setId(Long id) {
        this.id_area_voluntariado = id;
    }

}
