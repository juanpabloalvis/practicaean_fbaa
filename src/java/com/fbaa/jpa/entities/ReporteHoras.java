/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author INTERNET
 */
@Entity
@Table(name = "huella_registros")
public class ReporteHoras implements Serializable {
    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @OneToOne
    @JoinColumn(name = "id_huella", foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT), referencedColumnName = "cedula")    
    private Voluntario id_huella;
    @Type(type = "timestamp")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date Horaregistro;
    private String registroCompleto;

    public Voluntario getId_huella() {
        return id_huella;
    }

    public void setId_huella(Voluntario id_huella) {
        this.id_huella = id_huella;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getHoraregistro() {
        return Horaregistro;
    }

    public void setHoraregistro(Date Horaregistro) {
        this.Horaregistro = Horaregistro;
    }

    public String getRegistroCompleto() {
        return registroCompleto;
    }

    public void setRegistroCompleto(String registroCompleto) {
        this.registroCompleto = registroCompleto;
    }
    
}
