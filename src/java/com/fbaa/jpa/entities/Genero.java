/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author INTERNET
 */
@Entity
@Table(name = "TABLA_GENERO")
public class Genero implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_genero;
    private String nombreGenero;

    public Long getId() {
        return id_genero;
    }

    public void setId(Long id) {
        this.id_genero = id;
    }

    public String getNombreGenero() {
        return nombreGenero;
    }

    public void setNombreGenero(String nombreGenero) {
        this.nombreGenero = nombreGenero;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_genero != null ? id_genero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id_genero fields are not set
        if (!(object instanceof Genero)) {
            return false;
        }
        Genero other = (Genero) object;
        if ((this.id_genero == null && other.id_genero != null) || (this.id_genero != null && !this.id_genero.equals(other.id_genero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.fbaa.jpa.entities.Genero[ id=" + id_genero + " ]";
    }
    
}
