/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author INTERNET
 */
@Entity
@Table(name = "tabla_usuarios_registrados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TablaUsuariosRegistrados.findAll", query = "SELECT t FROM TablaUsuariosRegistrados t"),
    @NamedQuery(name = "TablaUsuariosRegistrados.findByIdUsuario", query = "SELECT t FROM TablaUsuariosRegistrados t WHERE t.idUsuario = :idUsuario"),
    @NamedQuery(name = "TablaUsuariosRegistrados.findByPassword", query = "SELECT t FROM TablaUsuariosRegistrados t WHERE t.password = :password")})
public class TablaUsuariosRegistrados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "ID_USUARIO")
    private String idUsuario;
    @Size(max = 255)
    private String password;

    public TablaUsuariosRegistrados() {
    }

    public TablaUsuariosRegistrados(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idUsuario != null ? idUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TablaUsuariosRegistrados)) {
            return false;
        }
        TablaUsuariosRegistrados other = (TablaUsuariosRegistrados) object;
        if ((this.idUsuario == null && other.idUsuario != null) || (this.idUsuario != null && !this.idUsuario.equals(other.idUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.fbaa.jpa.entities.TablaUsuariosRegistrados[ idUsuario=" + idUsuario + " ]";
    }
    
}
