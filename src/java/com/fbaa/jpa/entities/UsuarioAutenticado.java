/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author INTERNET
 */
@Entity
@Table(name = "TABLA_USUARIOS_REGISTRADOS")
public class UsuarioAutenticado implements Serializable {

    private static final long serialVersionUID = 4L;
    @Id
    @Column(name = "ID_USUARIO")
    private String usuario;
    private String password;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UsuarioAutenticado(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }

    public UsuarioAutenticado() {
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuario != null ? usuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioAutenticado)) {
            return false;
        }
        UsuarioAutenticado other = (UsuarioAutenticado) object;
        if ((this.usuario == null && other.usuario != null) || (this.usuario != null && !this.usuario.equals(other.usuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.fbaa.jpa.UsuarioAutenticado[ usuario=" + usuario + " ]";
    }

}
