/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author INTERNET
 */
@Entity
@Table(name = "TABLA_UNIVERSIDAD")
public class Universidad implements Serializable {

    @Id
    @GeneratedValue
    private Long id_universidad;
    private String nombreUniversidad;

    public String getNombreUniversidad() {
        return nombreUniversidad;
    }

    public void setNombreUniversidad(String nombreUniversidad) {
        this.nombreUniversidad = nombreUniversidad;
    }

    public Long getId() {
        return id_universidad;
    }

    public void setId(Long id) {
        this.id_universidad = id;
    }

}
