/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author INTERNET
 */
public class LinkHistorialHoras implements Serializable{

    private double totalHoras;
    private ArrayList<RegistroHoraDia> registrosHoras;

    public double getTotalHoras() {
        return totalHoras;
    }

    public void setTotalHoras(double totalHoras) {
        this.totalHoras = totalHoras;
    }

    public ArrayList<RegistroHoraDia> getRegistrosHoras() {
        return registrosHoras;
    }

    public void setRegistrosHoras(ArrayList<RegistroHoraDia> registrosHoras) {
        this.registrosHoras = registrosHoras;
    }

    public static class RegistroHoraDia implements Serializable{

        private BigInteger id_cedula;
        private Date fecha;
        private Timestamp maxRegistro;
        private Time tiempoGastado;

        public RegistroHoraDia() {
        }

        public RegistroHoraDia(BigInteger id_cedula, Date fecha, Timestamp maxRegistro, Time tiempoGastado) {
            this.id_cedula = id_cedula;
            this.fecha = fecha;
            this.maxRegistro = maxRegistro;
            this.tiempoGastado = tiempoGastado;
        }

        public BigInteger getId_cedula() {
            return id_cedula;
        }

        public void setId_cedula(BigInteger id_cedula) {
            this.id_cedula = id_cedula;
        }

        public Date getFecha() {
            return fecha;
        }

        public void setFecha(Date fecha) {
            this.fecha = fecha;
        }

        public Timestamp getMaxRegistro() {
            return maxRegistro;
        }

        public void setMaxRegistro(Timestamp maxRegistro) {
            this.maxRegistro = maxRegistro;
        }

        public Time getTiempoGastado() {
            return tiempoGastado;
        }

        public void setTiempoGastado(Time tiempoGastado) {
            this.tiempoGastado = tiempoGastado;
        }
    }

}
