/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.DAO;

import com.fbaa.jpa.entities.Genero;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author INTERNET
 */
@Named(value = "gestionarGeneroDAO")
@ApplicationScoped
public class GestionarGeneroDAO implements Serializable{

  //  private final FbaaHibernateUtil fbaaHibernateUtil = new FbaaHibernateUtil();

    /**
     * Creates a new instance of GestionarUniversidadesDAO
     */
    public GestionarGeneroDAO() {
    }

    public Genero obtenerGeneroByID(Long id) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Genero epsById = new Genero();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String queryString = "FROM Genero v where v.id = :idParam";
            Query query = session.createQuery(queryString);
            query.setLong("idParam", id);
            epsById = (Genero) query.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return epsById;
    }

    public ArrayList<Genero> obtenerGeneros() {
        String jpql = "FROM Genero u";
        ArrayList<Genero> arrayList = null;
        List<Genero> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(jpql);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();
            arrayList = new ArrayList<>(resultados.size());
            arrayList.addAll(resultados);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return arrayList;

    }

}
