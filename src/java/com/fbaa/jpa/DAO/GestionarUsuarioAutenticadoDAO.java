/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.DAO;

import com.fbaa.jpa.entities.UsuarioAutenticado;
import java.io.Serializable;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author INTERNET
 */
public class GestionarUsuarioAutenticadoDAO implements Serializable {

   // private final FbaaHibernateUtil fbaaHibernateUtil = new FbaaHibernateUtil();

    public UsuarioAutenticado usuarioAutenticado(String userName, String pass) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        UsuarioAutenticado usuarioAutenticado = new UsuarioAutenticado();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            usuarioAutenticado = (UsuarioAutenticado) session.load(UsuarioAutenticado.class, userName);
            if (usuarioAutenticado == null || (!usuarioAutenticado.getPassword().equals(pass))) {
                usuarioAutenticado = null;
            }
            tx.commit();
        } catch (HibernateException e) {
            usuarioAutenticado = null;
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return usuarioAutenticado;
    }

    public void eliminarUsuario(String userName) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            // TODO revisar que funcione
            session.delete(session.load(UsuarioAutenticado.class, userName));
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

}
