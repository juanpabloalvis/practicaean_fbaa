/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.DAO;

import com.fbaa.jpa.entities.EPS;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author INTERNET
 */
@Named(value = "gestionarEpsDAO")
@ApplicationScoped
public class GestionarEpsDAO implements Serializable{

   // private final FbaaHibernateUtil fbaaHibernateUtil = new FbaaHibernateUtil();

    /**
     * Creates a new instance of GestionarUniversidadesDAO
     */
    public GestionarEpsDAO() {
    }

    public EPS obtenerEPSByID(Long id) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        EPS epsById = new EPS();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String queryString = "FROM EPS v where v.id = :idParam";
            Query query = session.createQuery(queryString);
            query.setLong("idParam", id);
            epsById = (EPS) query.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return epsById;
    }

    public void eliminarEPS(EPS eps) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(obtenerEPSByID(eps.getId()));
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
//

    public void crearEPS(EPS eps) {

        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Long estado;
        try {
            tx = session.beginTransaction();
            estado = (Long) session.save(eps);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void actualizarEPS(EPS eps) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Long estado;
        try {
            tx = session.beginTransaction();
            session.update(eps);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public ArrayList<EPS> obtenerEPSs() {
        String jpql = "FROM EPS u";
        ArrayList<EPS> arrayList = null;
        List<EPS> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(jpql);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();
            arrayList = new ArrayList<>(resultados.size());
            arrayList.addAll(resultados);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return arrayList;

    }

}
