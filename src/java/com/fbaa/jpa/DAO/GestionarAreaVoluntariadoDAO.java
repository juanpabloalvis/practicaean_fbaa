/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.DAO;

import com.fbaa.jpa.entities.AreaVoluntariado;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author INTERNET
 */
@Named(value = "gestionarAreaVoluntariadoDAO")
@ApplicationScoped
public class GestionarAreaVoluntariadoDAO implements Serializable{

   // private final FbaaHibernateUtil fbaaHibernateUtil = new FbaaHibernateUtil();

    /**
     * Creates a new instance of GestionarUniversidadesDAO
     */
    public GestionarAreaVoluntariadoDAO() {
    }

    public AreaVoluntariado obtenerAreaVoluntariadoByID(Long id) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        AreaVoluntariado areaVoluntariadoById = new AreaVoluntariado();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String queryString = "FROM AreaVoluntariado v where v.id = :paramId";
            Query query = session.createQuery(queryString);
            query.setLong("paramId", id);
            areaVoluntariadoById = (AreaVoluntariado) query.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return areaVoluntariadoById;
    }

    public void eliminarAreaVoluntariado(AreaVoluntariado areaVoluntariado) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(obtenerAreaVoluntariadoByID(areaVoluntariado.getId()));
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
//

    public void crearAreaVoluntariado(AreaVoluntariado areaVoluntariado) {

        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Long estado;
        try {
            tx = session.beginTransaction();
            estado = (Long) session.save(areaVoluntariado);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void actualizarAreaVoluntariado(AreaVoluntariado areaVoluntariado) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(areaVoluntariado);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public ArrayList<AreaVoluntariado> obtenerAreasVoluntariado() {
        String jpql = "FROM AreaVoluntariado u";
        ArrayList<AreaVoluntariado> arrayList = null;
        List<AreaVoluntariado> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(jpql);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();
            arrayList = new ArrayList<>(resultados.size());
            arrayList.addAll(resultados);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return arrayList;

    }

}
