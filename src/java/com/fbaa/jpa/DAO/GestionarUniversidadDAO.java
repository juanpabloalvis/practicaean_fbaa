/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.DAO;

import com.fbaa.jpa.entities.Universidad;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author INTERNET
 */
@Named(value = "gestionarUniversidadesDAO")
@ApplicationScoped
public class GestionarUniversidadDAO implements Serializable{

    //private final FbaaHibernateUtil fbaaHibernateUtil = new FbaaHibernateUtil();

    /**
     * Creates a new instance of GestionarUniversidadesDAO
     */
    public GestionarUniversidadDAO() {
    }

    
    public Universidad obtenerUniversidadByID(Long id) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Universidad universidadById = new Universidad();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String queryString = "FROM Universidad v where v.id = :idCedula";
            Query query = session.createQuery(queryString);
            query.setLong("idCedula", id);
            universidadById = (Universidad) query.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return universidadById;
    }

    public void eliminarUniversidad(Universidad universidad) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(obtenerUniversidadByID(universidad.getId()));
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
//

    public void crearUniversidad(Universidad universidad) {

        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Long estado;
        try {
            tx = session.beginTransaction();
            estado = (Long) session.save(universidad);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void actualizarUniversidad(Universidad universidad) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.update(universidad);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public ArrayList<Universidad> obtenerUniversidades() {
        String jpql = "FROM Universidad u";
        ArrayList<Universidad> arrayList = null;
        List<Universidad> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(jpql);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();
            arrayList = new ArrayList<>(resultados.size());
            arrayList.addAll(resultados);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return arrayList;

    }

}
