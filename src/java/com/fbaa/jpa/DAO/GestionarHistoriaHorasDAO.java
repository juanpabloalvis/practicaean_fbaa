/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.DAO;

import com.fbaa.jpa.entities.LinkHistorialHoras;
import com.fbaa.jpa.entities.ReporteHoras;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author INTERNET
 */
@Named(value = "gestionarHistoriaHorasDAO")
@ApplicationScoped
public class GestionarHistoriaHorasDAO implements Serializable {

    //  private final FbaaHibernateUtil fbaaHibernateUtil = new FbaaHibernateUtil();
    /**
     * Creates a new instance of GestionarUniversidadesDAO
     */
    public GestionarHistoriaHorasDAO() {
    }

    public LinkHistorialHoras obtenerReporteHoras(Long id) {
        LinkHistorialHoras linkHistoria = new LinkHistorialHoras();
//        System.out.println("adentro");
        String jpql = "SELECT u.id_huella, DATE(u.Horaregistro), MAX(u.Horaregistro), TIMEDIFF(max(time(u.Horaregistro)),min(time(u.Horaregistro))) FROM huella_registros u WHERE  u.id_huella = :idParam GROUP BY u.id_huella, DATE(u.Horaregistro);";

        ArrayList<ReporteHoras> arrayList = null;
        List<Object[]> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createSQLQuery(jpql);
        query.setLong("idParam", id);

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();

            BigInteger id_cedula;
            Date fecha;
            Timestamp maxRegistro;
            Time tiempoGastado;
            
            ArrayList<LinkHistorialHoras.RegistroHoraDia> soloListaRegistros = new ArrayList<>();
            LinkHistorialHoras.RegistroHoraDia registro = new LinkHistorialHoras.RegistroHoraDia();
            int totalHorasEnSegundos = 0;
            for (Object report[] : resultados) {
                registro = new LinkHistorialHoras.RegistroHoraDia();

                id_cedula = (BigInteger) report[0];
                fecha = (Date) report[1];
                maxRegistro = (Timestamp) report[2];
                tiempoGastado = (Time) report[3];
                registro.setId_cedula(id_cedula);
                registro.setFecha(fecha);
                registro.setMaxRegistro(maxRegistro);
                registro.setTiempoGastado(tiempoGastado);

                soloListaRegistros.add(registro);

                //objects = (Object[]) report[4];
                // System.out.println("1+-+->" + id_cedula + "2+-+->" + fecha + "3+-+->" + maxRegistro + "4+-+->" + tiempoGastado);
                totalHorasEnSegundos += tiempoGastado.getHours() * 3600 + tiempoGastado.getMinutes() * 60 + tiempoGastado.getSeconds();
//                Integer id = (Integer) objects[0];
//                String name = (String) objects[1];
//                log.debug("Club Id: " + id + " Name: " + name);
            }
            if (soloListaRegistros.size() > 0) {

                linkHistoria.setRegistrosHoras(soloListaRegistros);
                if(totalHorasEnSegundos!=0){
                    linkHistoria.setTotalHoras((totalHorasEnSegundos/60/60));
                }else{
                    linkHistoria.setTotalHoras(totalHorasEnSegundos);
                }
                
//                System.out.println("---->" + linkHistoria.getRegistrosHoras().get(1).getId_cedula());
//                System.out.println("---->" + linkHistoria.getTotalHoras());
            }

            arrayList = new ArrayList<>(resultados.size());

            //    tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return linkHistoria;

    }

    public LinkHistorialHoras obtenerReporteFecha(Long id) {
        LinkHistorialHoras linkHistoria = new LinkHistorialHoras();
//        System.out.println("adentro");
        String jpql = "SELECT u.id_huella, DATE(u.Horaregistro), MAX(u.Horaregistro), TIMEDIFF(max(time(u.Horaregistro)),min(time(u.Horaregistro))) FROM huella_registros u WHERE  u.id_huella = :idParam GROUP BY u.id_huella, DATE(u.Horaregistro);";

        ArrayList<ReporteHoras> arrayList = null;
        List<Object[]> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createSQLQuery(jpql);
        query.setLong("idParam", id);

        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();

            BigInteger id_cedula;
            Date fecha;
            Timestamp maxRegistro;
            Time tiempoGastado;
            
            ArrayList<LinkHistorialHoras.RegistroHoraDia> soloListaRegistros = new ArrayList<>();
            LinkHistorialHoras.RegistroHoraDia registro = new LinkHistorialHoras.RegistroHoraDia();
            int totalHorasEnSegundos = 0;
            for (Object report[] : resultados) {
                registro = new LinkHistorialHoras.RegistroHoraDia();

                id_cedula = (BigInteger) report[0];
                fecha = (Date) report[1];
                maxRegistro = (Timestamp) report[2];
                tiempoGastado = (Time) report[3];
                registro.setId_cedula(id_cedula);
                registro.setFecha(fecha);
                registro.setMaxRegistro(maxRegistro);
                registro.setTiempoGastado(tiempoGastado);

                soloListaRegistros.add(registro);

                //objects = (Object[]) report[4];
                // System.out.println("1+-+->" + id_cedula + "2+-+->" + fecha + "3+-+->" + maxRegistro + "4+-+->" + tiempoGastado);
                totalHorasEnSegundos += tiempoGastado.getHours() * 3600 + tiempoGastado.getMinutes() * 60 + tiempoGastado.getSeconds();
//                Integer id = (Integer) objects[0];
//                String name = (String) objects[1];
//                log.debug("Club Id: " + id + " Name: " + name);
            }
            if (soloListaRegistros.size() > 0) {

                linkHistoria.setRegistrosHoras(soloListaRegistros);
                if(totalHorasEnSegundos!=0){
                    linkHistoria.setTotalHoras((totalHorasEnSegundos/60/60));
                }else{
                    linkHistoria.setTotalHoras(totalHorasEnSegundos);
                }
                
//                System.out.println("---->" + linkHistoria.getRegistrosHoras().get(1).getId_cedula());
//                System.out.println("---->" + linkHistoria.getTotalHoras());
            }

            arrayList = new ArrayList<>(resultados.size());

            //    tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return linkHistoria;

    }

}
