/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.jpa.DAO;

import com.fbaa.jpa.entities.AreaVoluntariado;
import com.fbaa.jpa.entities.Universidad;
import com.fbaa.jpa.entities.Voluntario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author INTERNET
 */
public class GestionarVoluntarioDAO implements Serializable {

   // private final FbaaHibernateUtil fbaaHibernateUtil = new FbaaHibernateUtil();
    public Voluntario obtenerVoluntario(Long idVolunatario) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Voluntario voluntarioById = new Voluntario();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            voluntarioById = (Voluntario) session.load(Voluntario.class, idVolunatario);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return voluntarioById;
    }

    public Voluntario obtenerVoluntarioPorDocumentoIdentidad(Long cedula) {

        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Voluntario voluntarioById = new Voluntario();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            String queryString = "FROM Voluntario v where v.cedula = :idCedula";
            Query query = session.createQuery(queryString);
            query.setLong("idCedula", cedula);
            voluntarioById = (Voluntario) query.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return voluntarioById;
    }

    public void eliminarVoluntario(Voluntario volunatario) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(obtenerVoluntario(volunatario.getId()));
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
//

    public void guardarVoluntario(Voluntario volunatario) {

        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Long estado;
        try {
            tx = session.beginTransaction();
            estado = (Long) session.save(volunatario);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public void actualizarVoluntario(Voluntario volunatario) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        Long estado;
        try {
            tx = session.beginTransaction();
            session.update(volunatario);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

    }

    public ArrayList<Voluntario> obtenerVoluntarios() {
        String jpql = "FROM Voluntario v";
        ArrayList<Voluntario> arrayList = null;
        List<Voluntario> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(jpql);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();
            arrayList = new ArrayList<>(resultados.size());
            arrayList.addAll(resultados);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return arrayList;

    }

    public ArrayList<Universidad> obtenerUniversidades() {
        String jpql = "FROM Universidad u";
        ArrayList<Universidad> arrayList = null;
        List<Universidad> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(jpql);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();
            arrayList = new ArrayList<>(resultados.size());
            arrayList.addAll(resultados);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return arrayList;

    }

    public ArrayList<AreaVoluntariado> obtenerAreasVoluntariado() {
        String jpql = "FROM AreaVoluntariado a";
        ArrayList<AreaVoluntariado> arrayList = null;
        List<AreaVoluntariado> resultados;
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery(jpql);
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            resultados = query.list();
            arrayList = new ArrayList<>(resultados.size());
            arrayList.addAll(resultados);

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return arrayList;

    }

    public AreaVoluntariado obtenerAreaVoluntariado(Long idVolunatario) {
        Session session = FbaaHibernateUtil.getSessionFactory().openSession();
        AreaVoluntariado voluntarioById = new AreaVoluntariado();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            voluntarioById = (AreaVoluntariado) session.load(AreaVoluntariado.class, idVolunatario);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return voluntarioById;
    }

}
