/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.util;

import java.io.Serializable;

/**
 *
 * @author INTERNET
 */
public class ReporteVoluntarioJasper implements Serializable {

    private String cedula;
    private String nombre;
    private String apellido;
    private Integer horas;
    private String valPlan;
    private String valPunt;
    private String valRelaci;
    private String valCumpli;
    private String valLogro;
    private String valTotal;

    public ReporteVoluntarioJasper(String cedula, String nombre, String apellido, Integer horas, String valPlan, String valPunt, String valRelaci, String valCumpli, String valLogro, String valTotal) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.horas = horas;
        this.valPlan = valPlan;
        this.valPunt = valPunt;
        this.valRelaci = valRelaci;
        this.valCumpli = valCumpli;
        this.valLogro = valLogro;
        this.valTotal = valTotal;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Integer getHoras() {
        return horas;
    }

    public void setHoras(Integer horas) {
        this.horas = horas;
    }

    public String getValPlan() {
        return valPlan;
    }

    public void setValPlan(String valPlan) {
        this.valPlan = valPlan;
    }

    public String getValPunt() {
        return valPunt;
    }

    public void setValPunt(String valPunt) {
        this.valPunt = valPunt;
    }

    public String getValRelaci() {
        return valRelaci;
    }

    public void setValRelaci(String valRelaci) {
        this.valRelaci = valRelaci;
    }

    public String getValCumpli() {
        return valCumpli;
    }

    public void setValCumpli(String valCumpli) {
        this.valCumpli = valCumpli;
    }

    public String getValLogro() {
        return valLogro;
    }

    public void setValLogro(String valLogro) {
        this.valLogro = valLogro;
    }

    public String getValTotal() {
        return valTotal;
    }

    public void setValTotal(String valTotal) {
        this.valTotal = valTotal;
    }

    public ReporteVoluntarioJasper() {
    }
}
