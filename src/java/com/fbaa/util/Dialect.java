/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.util;

/**
 *
 * @author INTERNET
 */
enum Dialect {

    MYSQL("org.hibernate.dialect.MySQLDialect"),
    ORACLE("org.unhcr.omss.db.oracle.OracleDialectDeferredFK"),
    SYBASE("org.hibernate.dialect.SybaseAnywhereDialect");

    private String className;

    private Dialect(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }
}
