/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.util;

import com.fbaa.jpa.entities.Universidad;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.tool.hbm2ddl.SchemaExport;

/**
 *
 * @author INTERNET
 */
public class HibernateDDLGenerator {

    public static void main(String[] args) {
        new HibernateDDLGenerator().execute(Dialect.MYSQL, Universidad.class);
    }

    private void execute(Dialect dialect, Class<?>... classes) {
        // AnnotationConfiguration configuration = new AnnotationConfiguration();
        Configuration configuration = new Configuration();
        configuration.configure("/hibernate.cfg.xml");
        configuration.setProperty(Environment.DIALECT, dialect.getClassName());
        for (Class<?> entityClass : classes) {
            configuration.addAnnotatedClass(entityClass);
        }
        SchemaExport schemaExport = new SchemaExport(configuration);
        schemaExport.setDelimiter(";");
        // http://www.programcreek.com/java-api-examples/index.php?api=org.hibernate.tool.hbm2ddl.SchemaExport
        schemaExport.setOutputFile(String.format("%s_%s.%s ", new Object[]{"ddl", dialect.name().toLowerCase(), "sql"}));
        //schema.setOutputFile("../conf/database/db-001-schema.sql");
        boolean consolePrint = true;
        boolean exportInDatabase = false;
        schemaExport.create(consolePrint, exportInDatabase);
    }
}
