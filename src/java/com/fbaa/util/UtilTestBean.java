/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fbaa.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Juan Pablo Alvis <juanpabloalvis@gmail.com>
 */
/**
 *
 * @author Juan Pablo Alvis <juanpabloalvis@gmail.com>
 */
@ManagedBean(name = "utils") //@Named este no funciona porque no se usa CFI
@SessionScoped
public class UtilTestBean implements Serializable {

    private List<PizzaObject> results = new ArrayList<>();
    private String nombre;
    static final Logger LOGGER = Logger.getLogger(UtilTestBean.class.getName());

    public UtilTestBean() {
        LOGGER.info("Iniciando Bean");
        List<PizzaObject.Size> pizaSize = new ArrayList<>();
        pizaSize.add(new PizzaObject.Size(1, 1));
        pizaSize.add(new PizzaObject.Size(2, 2));
        pizaSize.add(new PizzaObject.Size(3, 3));
        List<PizzaObject.Topping> pizaTopping = new ArrayList<>();
        pizaTopping.add(new PizzaObject.Topping(4, 4));
        pizaTopping.add(new PizzaObject.Topping(5, 5));
        pizaTopping.add(new PizzaObject.Topping(6, 6));
        PizzaObject.Pizza piza = new PizzaObject.Pizza();
        piza.setNombre("peperoni");
        piza.setSalsa("de la cas");
        PizzaObject pizzaObject = new PizzaObject();
        pizzaObject.setPizza(piza);
        pizzaObject.setSizeList(pizaSize);
        pizzaObject.setToppingList(pizaTopping);
        nombre = "";
        results.add(pizzaObject);
        results.add(pizzaObject);
    }

    public List<PizzaObject> getResults() {
        return results;
    }

    public void setResults(List<PizzaObject> results) {
        this.results = results;
    }

    public String getNombre() {
        return ResourcesUtil.getString("#{msg['app.saludo']}") + nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void getNombreFromAjax() {

    }

    // CLASES ESTATICAS PARA RADIO BUTTON ANIDADO
    public static class PizzaObject {

        private Pizza pizza;
        private List<Size> sizeList;
        private List<Topping> toppingList;

        public PizzaObject() {
        }

        public PizzaObject(Pizza pizza, List<Size> sizeList, List<Topping> toppingList) {
            this.pizza = pizza;
            this.sizeList = sizeList;
            this.toppingList = toppingList;
        }

        public Pizza getPizza() {
            return pizza;
        }

        public void setPizza(Pizza pizza) {
            this.pizza = pizza;
        }

        public List<Size> getSizeList() {
            return sizeList;
        }

        public void setSizeList(List<Size> sizeList) {
            this.sizeList = sizeList;
        }

        public List<Topping> getToppingList() {
            return toppingList;
        }

        public void setToppingList(List<Topping> toppingList) {
            this.toppingList = toppingList;
        }

        public static class Size {

            public int sizeID;
            public int diameter;

            public Size() {
            }

            
            public Size(int sizeID, int diameter) {
                this.sizeID = sizeID;
                this.diameter = diameter;
            }

            // getter and setter
            public int getSizeID() {
                return sizeID;
            }

            public void setSizeID(int sizeID) {
                this.sizeID = sizeID;
            }

            public int getDiameter() {
                return diameter;
            }

            public void setDiameter(int diameter) {
                this.diameter = diameter;
            }
        }

        public static class Topping {

            public int sizeID;
            public int diameter;

            public Topping(int sizeID, int diameter) {
                this.sizeID = sizeID;
                this.diameter = diameter;
            }

            // getter and setter
            public int getSizeID() {
                return sizeID;
            }

            public void setSizeID(int sizeID) {
                this.sizeID = sizeID;
            }

            public int getDiameter() {
                return diameter;
            }

            public void setDiameter(int diameter) {
                this.diameter = diameter;
            }
        }

        public static class Pizza {

            private String nombre;
            private String salsa;

            public Pizza() {
            }

            public Pizza(String nombre, String salsa) {
                this.nombre = nombre;
                this.salsa = salsa;
            }

            public String getNombre() {
                return nombre;
            }

            public void setNombre(String nombre) {
                this.nombre = nombre;
            }

            public String getSalsa() {
                return salsa;
            }

            public void setSalsa(String salsa) {
                this.salsa = salsa;
            }

        }
    }

}
