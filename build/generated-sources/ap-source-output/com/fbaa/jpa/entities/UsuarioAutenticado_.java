package com.fbaa.jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UsuarioAutenticado.class)
public abstract class UsuarioAutenticado_ {

	public static volatile SingularAttribute<UsuarioAutenticado, String> password;
	public static volatile SingularAttribute<UsuarioAutenticado, String> usuario;

}

