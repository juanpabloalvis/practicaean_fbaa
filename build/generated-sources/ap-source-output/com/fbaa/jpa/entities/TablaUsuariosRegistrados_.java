package com.fbaa.jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TablaUsuariosRegistrados.class)
public abstract class TablaUsuariosRegistrados_ {

	public static volatile SingularAttribute<TablaUsuariosRegistrados, String> password;
	public static volatile SingularAttribute<TablaUsuariosRegistrados, String> idUsuario;

}

