package com.fbaa.jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(AreaVoluntariado.class)
public abstract class AreaVoluntariado_ {

	public static volatile SingularAttribute<AreaVoluntariado, Long> id_area_voluntariado;
	public static volatile SingularAttribute<AreaVoluntariado, String> nombreAreaVoluntariado;

}

