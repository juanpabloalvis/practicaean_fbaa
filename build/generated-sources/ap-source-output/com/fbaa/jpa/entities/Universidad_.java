package com.fbaa.jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Universidad.class)
public abstract class Universidad_ {

	public static volatile SingularAttribute<Universidad, String> nombreUniversidad;
	public static volatile SingularAttribute<Universidad, Long> id_universidad;

}

