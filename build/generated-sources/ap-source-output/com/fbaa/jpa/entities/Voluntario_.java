package com.fbaa.jpa.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Voluntario.class)
public abstract class Voluntario_ {

	public static volatile SingularAttribute<Voluntario, String> apellidos;
	public static volatile SingularAttribute<Voluntario, AreaVoluntariado> areaVoluntariado;
	public static volatile SingularAttribute<Voluntario, Long> cedula;
	public static volatile SingularAttribute<Voluntario, String> nivelEstudios;
	public static volatile SingularAttribute<Voluntario, EPS> EPS;
	public static volatile SingularAttribute<Voluntario, Universidad> universidad;
	public static volatile SingularAttribute<Voluntario, String> nombres;
	public static volatile SingularAttribute<Voluntario, Date> fechaIngreso;
	public static volatile SingularAttribute<Voluntario, Integer> intensidadHoras;
	public static volatile SingularAttribute<Voluntario, String> horarios;
	public static volatile SingularAttribute<Voluntario, String> correo;
	public static volatile SingularAttribute<Voluntario, Genero> genero;
	public static volatile SingularAttribute<Voluntario, String> dias;
	public static volatile SingularAttribute<Voluntario, Long> id;
	public static volatile SingularAttribute<Voluntario, Long> telefono;
	public static volatile SingularAttribute<Voluntario, String> carrera;

}

