package com.fbaa.jpa.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ReporteHoras.class)
public abstract class ReporteHoras_ {

	public static volatile SingularAttribute<ReporteHoras, Voluntario> id_huella;
	public static volatile SingularAttribute<ReporteHoras, Date> Horaregistro;
	public static volatile SingularAttribute<ReporteHoras, Long> id;
	public static volatile SingularAttribute<ReporteHoras, String> registroCompleto;

}

