package com.fbaa.jpa.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Genero.class)
public abstract class Genero_ {

	public static volatile SingularAttribute<Genero, String> nombreGenero;
	public static volatile SingularAttribute<Genero, Long> id_genero;

}

