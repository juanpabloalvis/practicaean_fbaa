
    alter table TABLA_VOLUNTARIOS 
        drop 
        foreign key FK_4u6mqerf3iisp6vaqyvbijk0y;

    alter table TABLA_VOLUNTARIOS 
        drop 
        foreign key FK_ftgf1meeque9wpq0ge1h10pxa;

    alter table TABLA_VOLUNTARIOS 
        drop 
        foreign key FK_omeyt3632dg4oh30c71q86bb4;

    alter table TABLA_VOLUNTARIOS 
        drop 
        foreign key FK_hvg60qu6yd3k125mbinjvs1nc;

    alter table huella_registros 
        drop 
        foreign key FK_n4vsxujtoomx2hamr5at0htv6;

    drop table if exists TABLA_AREA_VOLUNTARIADO;

    drop table if exists TABLA_EPS;

    drop table if exists TABLA_GENERO;

    drop table if exists TABLA_UNIVERSIDAD;

    drop table if exists TABLA_USUARIOS_REGISTRADOS;

    drop table if exists TABLA_VOLUNTARIOS;

    drop table if exists huella_registros;

    create table TABLA_AREA_VOLUNTARIADO (
        id_area_voluntariado bigint not null auto_increment,
        nombreAreaVoluntariado varchar(255),
        primary key (id_area_voluntariado)
    );

    create table TABLA_EPS (
        id bigint not null auto_increment,
        nombreEPS varchar(255),
        primary key (id)
    );

    create table TABLA_GENERO (
        id_genero bigint not null auto_increment,
        nombreGenero varchar(255),
        primary key (id_genero)
    );

    create table TABLA_UNIVERSIDAD (
        id_universidad bigint not null auto_increment,
        nombreUniversidad varchar(255),
        primary key (id_universidad)
    );

    create table TABLA_USUARIOS_REGISTRADOS (
        ID_USUARIO varchar(255) not null,
        password varchar(255),
        primary key (ID_USUARIO)
    );

    create table TABLA_VOLUNTARIOS (
        ID_VOLUNTARIO bigint not null auto_increment,
        apellidos varchar(255),
        carrera varchar(255),
        cedula bigint not null,
        correo varchar(255),
        dias varchar(8),
        fechaIngreso date,
        horarios varchar(255),
        intensidadHoras integer not null,
        nivelEstudios varchar(255),
        nombres varchar(255),
        telefono bigint not null,
        id_eps bigint,
        id_area_voluntariado bigint,
        id_genero bigint,
        id_universidad bigint,
        primary key (ID_VOLUNTARIO)
    );

    create table huella_registros (
        id bigint not null auto_increment,
        Horaregistro datetime,
        registroCompleto varchar(255),
        id_huella bigint,
        primary key (id)
    );

    alter table TABLA_VOLUNTARIOS 
        add constraint UK_neir3w1e1v5avgup8adyhoybv  unique (cedula);

    alter table TABLA_VOLUNTARIOS 
        add constraint FK_4u6mqerf3iisp6vaqyvbijk0y 
        foreign key (id_eps) 
        references TABLA_EPS (id);

    alter table TABLA_VOLUNTARIOS 
        add constraint FK_ftgf1meeque9wpq0ge1h10pxa 
        foreign key (id_area_voluntariado) 
        references TABLA_AREA_VOLUNTARIADO (id_area_voluntariado);

    alter table TABLA_VOLUNTARIOS 
        add constraint FK_omeyt3632dg4oh30c71q86bb4 
        foreign key (id_genero) 
        references TABLA_GENERO (id_genero);

    alter table TABLA_VOLUNTARIOS 
        add constraint FK_hvg60qu6yd3k125mbinjvs1nc 
        foreign key (id_universidad) 
        references TABLA_UNIVERSIDAD (id_universidad);

    alter table huella_registros 
        add constraint FK_n4vsxujtoomx2hamr5at0htv6 
        foreign key (id_huella) 
        references TABLA_VOLUNTARIOS (cedula);
